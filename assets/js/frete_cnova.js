$(document).ready(function(){
    console.log(store);
    console.log(BASE_URL);
    console.log(SKU);
    function get_product_details()
    {
        var produto = null;
        console.log(BASE_URL+'app/product/get_details/'+store+'/'+SKU);
        var request = $.ajax({ 
            type : "GET",
            async: false,
            url: BASE_URL+'app/product/get_details/'+store+'/'+SKU+'/json/', 
            cache : false
        });
        request.always(function(res)
        {
            produto = JSON.parse(res);
        });
        return produto;
    }

    var details = get_product_details();
    //console.log(details);

    $(".select-frete").change(function(){
        $('.frete tbody').html('');
        var estado = $(this).val().toLowerCase();
        var lista_cep = get_region_zip_code(estado);

        $("#money").inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ",",
            'digitsOptional': false,
            'allowMinus': false,
            'prefix': 'R$ ',
            'placeholder': ''
        });


        var lista_cep_cidades = [
        'Capital',
        'Interior',
        ];

        console.log(lista_cep);
        console.log(lista_cep.length);
        for(var i=0; i<lista_cep.length; i++){

            var html = "<tr><td colspan=6 style='background-color: #f5f7fa;text-align: center;font-size: 2rem;'><strong>" + lista_cep_cidades[i] + "</strong></tr>";
            $('.frete tbody').append(html);
            for(var x=0; x<details.sku_competitors.length; x++){
                console.log(details.sku_competitors[x].id_loja);
                console.log(details.sku_competitors[x]);
                console.log('prefixo='+lista_cep[i].substring(0,4)+'&sufixo='+lista_cep[i].substring(5,7)+'&idLojista='+details.sku_competitors[x].id_loja+'&sku='+SKU);
                id_loja         = details.sku_competitors[x].id_loja;
                var nome_loja   = details.sku_competitors[x].loja;
                console.log(BASE_URL+'app/product/get_shipping_values/'+store+'/'+SKU+'/'+lista_cep[i]);
                var request = $.ajax({ 
                    type : "GET",
                    //dataType: 'json',
                    async: false,
                    crossDomain: true,
                    //data: 'prefixo='+lista_cep[i].substring(0,5)+'&sufixo='+lista_cep[i].substring(5,8)+'&idLojista='+details.sku_competitors[x].id_loja+'&sku='+SKU,
                    url: BASE_URL+'app/product/get_shipping_values/'+store+'/'+SKU+'/'+lista_cep[i], 
                    cache : false
                });

                request.done(function(res)
                {

                    res = JSON.parse(res);

                    for(var y=0; y<res.length; y++){
                        var preco_frete = res[y].preco;
                        var tipo_frete  = res[y].tipo_frete;
                        //tipo_frete      = get_freight_name(tipo_frete);
                        carrinho = parseFloat(details.sku_competitors[x].preco) + parseFloat(preco_frete);
                        carrinho = "R$"+numeroParaMoeda(carrinho);
                        if(preco_frete == "0"){
                        preco_frete = "Grátis";
                        }else{
                        preco_frete = "R$"+numeroParaMoeda(preco_frete);
                        }
                        
                        var html = "<tr><td>"+nome_loja+"</td><td>"+tipo_frete+"</td><td>"+preco_frete+"</td><td>"+res[y].max_prazo_entrega+" dias(s)</td><td>"+carrinho+"</td></tr>";
                        $('.frete tbody').append(html);
                    }
                });
                
                request.fail(function(res)
                {
                    console.log(res);
                    var tipo_frete  = res.responseJSON.message;
                    tipo_frete      = get_freight_name(tipo_frete);
                    var markup = "<tr><td>"+nome_loja+"</td><td colspan='5'>"+tipo_frete+"</td></tr>";
                    $('.frete').append(markup);
                });
            }
            
        }


    });


    function get_freight_name(tipo_frete){
      switch(tipo_frete) {
          case 'CONVENTIONAL':
            return "Convencional";
            break;
          case 'CONVENTIONALPRIME':
            return "Prime";
            break;
          case 'Produtos não entregues em sua região.':
            return "Região não atendida.";
            break;
          default:
            return tipo_frete;
      }
    }

    function get_region_zip_code(region)
    {
        switch(region){
            case "sp":
            return lista_cep = [
                '03182050', //São Paulo - Capital
                '18085844', //São Paulo - Interior
            ];
            break;

            case "mg":
            return lista_cep = [
                '31270901', //Minas Gerais - Capital
                '38402100', //Minas Gerais - Interior
            ];
            break;

            case "rj":
            return lista_cep = [
                '20011901', //Rio de Janeiro - Capital
                '23017410', //Rio de Janeiro - Interior
            ];
            break;

            case "es":
            return lista_cep = [
                '29045402', //Espírito Santo - Capital
                '29190010', //Espírito Santo - Interior
            ];
            break;


            case "pr":
            return lista_cep = [
                '81170423', //Paraná - Capital
		        '83511210', //Paraná - Interior
            ];
            break;

            case "sc":
            return lista_cep = [
                '88049120', //SAnta Catarina - Capital
		        '89809704', //SAnta Catarina - Interior
            ];
            break;

            case "rs":
            return lista_cep = [
                '91510860', //Rio Grande do Sul - Capital
		        '99074170', //Rio Grande do Sul - Interior
            ];
            break;

            case "df":
            return lista_cep = [
                '70750503', //DF - Brasília
		        '72705030', //DF - Interior
            ];
            break;

            case "go":
            return lista_cep = [
                '74710310', //Goiás - Capital
		        '75110380', //Goiás - Interior
            ];
            break;

            case "mt":
            return lista_cep = [
                '78065825', //Mato Grosso - Capital
		        '78746153', //Mato Grosso - Interior
            ];
            break;

            case "ms":
            return lista_cep = [
                '79112251', //Mato Grosso do Sul - Capital
		        '79904224', //Mato Grosso do Sul - Interior
            ];
            break;

            case "al":
            return lista_cep = [
                '57084042', //Alagoas - Capital
		        '57600220', //Alagoas - Interior
            ];
            break;

            case "ba":
            return lista_cep = [
                '41195000', //Bahia - Capital
		        '48090030', //Bahia - Interior
            ];
            break;

            case "ce":
            return lista_cep = [
                '60864390', //Ceará - Capital
		        '63047200', //Ceará - Interior
            ];
            break;

            case "ma":
            return lista_cep = [
                '65050730', //Maranhão - Capital
		        '65915600', //Maranhão - Interior
            ];
            break;

            case "pb":
            return lista_cep = [
                '58037310', //Paraíba - Capital
		        '58300750', //Paraíba - Interior
            ];
            break;

            case "pe":
            return lista_cep = [
                '50790040', //Pernambuco - Capital
		        '56328400', //Pernambuco - Interior
            ];
            break;

            case "pi":
            return lista_cep = [
                '64031050', //Piauí - Capital
		        '64605135', //Piauí - Interior
            ];
            break;

            case "se":
            return lista_cep = [
                '49000707', //Sergipe - Capital
		        '49209509', //Sergipe - Interior
            ];
            break;

            case "ac":
            return lista_cep = [
                '69907704', //Acre - Capital
		        '69932000', //Acre - Interior
            ];
            break;

            case "ap":
            return lista_cep = [
                '68903758', //Amapá - Capital
		        '68926166', //Amapá - Interior
            ];
            break;

            case "am":
            return lista_cep = [
                '69082051', //Amazonas - Capital
		        '69400250', //Amazonas - Interior
            ];
            break;

            case "pa":
            return lista_cep = [
                '66040146', //Pará - Capital
		        '67145043', //Pará - Interior
            ];
            break;

            case "ro":
            return lista_cep = [
                '76808136', //Rondônia - Capital
		        '76900560', //Rondônia - Interior
            ];
            break;

            case "rr":
            return lista_cep = [
                '69305125', //Roraima - Capital
		        '69395000', //Roraima - Interior
            ];
            break;

            case "to":
            return lista_cep = [
                '77020163', //Tocantins - Capital
		        '77808600', //Tocantins - Interior
            ];
            break;

            
        }

    }
    });