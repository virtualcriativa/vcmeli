//var base_url = window.location.origin;

function gd(year, month, day) {
	return new Date(year, month - 1, day).getTime();
}

function format (d) {
	return '<div class="col-md-12 col-sm-12 col-xs-12"><p>Histórico de preço</p><div class="demo-container" style="height:100%; height: 200px; width: 100%; margin: 22px 0;"><div id="chart_plot_b2w" class="demo-placeholder" style="height:100%; height: 200px; width: 100%"></div></div></div>';
}

$(document).ready(function() {
	
	$("a.perdendo").click(function(){
			$("#filtro_perdendo").submit();
	});
	$("a.ganhando").click(function(){
			$("#filtro_ganhando").submit();
	});
	$("a.exclusivo").click(function(){
			$("#filtro_exclusivo").submit();
	});
	$("a.esgotado").click(function(){
			$("#filtro_esgotado").submit();
	});
	
	//console.log("get_prices/"+store);
	if($('#datatable').hasClass('datatable')){
		var datatable_url = BASE_URL+"app/get_prices/"+store;
	}else if($('#datatable').hasClass('datatable_losing')){
		var datatable_url = BASE_URL+"app/get_prices/"+store+"/losing";
	}else if($('#datatable').hasClass('datatable_exclusive')){
		var datatable_url = BASE_URL+"app/get_prices/"+store+"/exclusive";
	}else if($('#datatable').hasClass('datatable_winning')){
		var datatable_url = BASE_URL+"app/get_prices/"+store+"/winning";
	}else if($('#datatable').hasClass('datatable_sold_off')){
		var datatable_url = BASE_URL+"app/get_prices/"+store+"/out";
	}

	var table = $('#datatable').DataTable({
					"language": {
						"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
					},
					"order": [[ 2, "desc" ]],
					"processing": true,
					"serverSide": true,
					"ajax": {
						type: 'GET',
						url: datatable_url
					},
					"columns": [
						{
							"className":      'details-control',
							"orderable":      false,
							"data":           null,
							"defaultContent": '<i class="fas fa-chart-line"></i><i class="fas fa-times-circle" style="display:none"></i>'
						},
						{ "data": "Img",
							"render": function(data, type, row) {
								return '<img src="'+data+'" width=50 />';
							}
						},
						{ "data": "SKU", "className": 'prod-sku' },
						{ "data": "produto", "className": 'prod-name' },
						{ "data": "preco", "className": 'prod-preco' },
						{ "data": "ordem",
							"render": function(data, type, row) {
								if(data===null){
									return "Sem estoque";
								}else{
									return '<div class="ordem_'+data+'">'+data+'</div>';
								}
							}
						},
						{ "className": "concorrentes", "data": "concorrente",
						"render": function(data, type, row) {
								return '<div class="competition">'+data+'</div>';
							} 
						}
					]
				});
				
} );

	