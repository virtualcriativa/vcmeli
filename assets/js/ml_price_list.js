//var base_url = window.location.origin;

function gd(year, month, day) {
	return new Date(year, month - 1, day).getTime();
}

function format (d) {
	return '<div class="col-md-12 col-sm-12 col-xs-12"><p>Histórico de preço</p><div class="demo-container" style="height:100%; height: 200px; width: 100%; margin: 22px 0;"><div id="chart_plot_b2w" class="demo-placeholder" style="height:100%; height: 200px; width: 100%"></div></div></div>';
}

$(document).ready(function() {
	
	$("a.perdendo").click(function(){
			$("#filtro_perdendo").submit();
	});
	$("a.ganhando").click(function(){
			$("#filtro_ganhando").submit();
	});
	$("a.exclusivo").click(function(){
			$("#filtro_exclusivo").submit();
	});
	$("a.esgotado").click(function(){
			$("#filtro_esgotado").submit();
	});
	
	//console.log("get_prices/"+store);
	if($('#datatable').hasClass('datatable')){
		var datatable_url = BASE_URL+"mercadolivre/get_prices/"+store;
	}else if($('#datatable').hasClass('datatable_losing')){
		var datatable_url = BASE_URL+"mercadolivre/get_prices/"+store+"/losing";
	}else if($('#datatable').hasClass('datatable_exclusive')){
		var datatable_url = BASE_URL+"mercadolivre/get_prices/"+store+"/exclusive";
	}else if($('#datatable').hasClass('datatable_winning')){
		var datatable_url = BASE_URL+"mercadolivre/get_prices/"+store+"/winning";
	}else if($('#datatable').hasClass('datatable_sold_off')){
		var datatable_url = BASE_URL+"mercadolivre/get_prices/"+store+"/out";
    }

	var table = $('#datatable').DataTable({
					"language": {
						"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
					},
					"order": [[ 2, "desc" ]],
					"processing": true,
					"serverSide": true,
					"ajax": {
						type: 'GET',
						url: datatable_url
					},
					"columns": [
						{
							"className":      'details-control',
							"orderable":      false,
							"data":           null,
							"defaultContent": '<i class="fas fa-chart-line"></i><i class="fas fa-times-circle" style="display:none"></i>'
						},
						{ "data": "Img",
							"render": function(data, type, row) {
								return '<img src="'+data+'" width=50 />';
							}
						},
						{ "data": "SKU", "className": 'prod-sku' },
						{ "data": "produto", "className": 'prod-name' },
						{ "data": "preco", "className": 'prod-preco' },
						{ "data": "ordem",
							"render": function(data, type, row) {
								if(data===null){
									return "Sem estoque";
								}else{
									return '<div class="ordem_'+data+'">'+data+'</div>';
								}
							}
						},
						{ "className": "concorrentes", "data": "concorrente",
						"render": function(data, type, row) {
								return '<div class="competition">'+data+'</div>';
							} 
						}
					]
				});
				
    
    // Add event listener for opening and closing details
    $('#datatable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
		var sku = $(tr).find('td')[2].innerText;
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
			
			
			//close all before open
			$('td.details-control').each(function(index){
				var tr = $(this).closest('tr');
				var row = table.row( tr );
				if ( row.child.isShown() ) {
					// This row is already open - close it
					row.child.hide();
					tr.removeClass('shown');
				}
			});
			
			
			
			
            // Open this row
			$("#chart_plot_b2w").remove();
			var price_hist_url = BASE_URL+"app/get_historic_prices/meli?sku="+sku;
			
			$.getJSON( price_hist_url, {
				format: "json"
			  })
				.done(function( data ) {
					console.log(data);
					if(data.data==""){
						alert("sem histórico para exibir no momento");
					}else{
						row.child( format(data) ).show();
						tr.addClass('shown');
						addChart(data);
					}
				})
				.fail(function( jqxhr, textStatus, error ) {
					var err = textStatus + ", " + error;
					console.log( "Request Failed: " + err );
				})
			
			
            
        }
    } );
} );

	
function addChart(data){
	var chart_plot_01_data= [];
	var chart_plot_data= [];
	var lojaArr = [];
	var min_price = 1000000000;
	$.each( data, function( key, value ) {
		
	  $.each(value, function( keyA, valueA ){
		  
			var loja = (valueA[0].loja).toLowerCase();
			if(loja==""){
				loja = "Seu Preço";
			}
			chart_plot_data[loja] = [];
			for (var i = 0; i < valueA.length; i++) {
				if(Number(valueA[i].preco)>0 && Number(valueA[i].preco)<min_price){
					min_price = Number(valueA[i].preco);
				}
				chart_plot_data[loja].push([new Date(valueA[i].data).getTime(), Number(valueA[i].preco)]);
			}
			lojaArr.push([loja]);

		});
	});
	//console.log(min_price);
	var chartinfo = [];
	$.each( lojaArr, function( key, value ) {
		chartinfo.push(
			{
				label : value, 
				data : chart_plot_data[value], 
				lines: { fillColor: "rgba(150, 202, 89, 0.01)" }, 
				points: { fillColor: "#fff" } 
			}
		);

	});

	var chart_plot_02_settings = {
			grid: {
				show: true,
				aboveData: true,
				color: "#3f3f3f",
				labelMargin: 10,
				axisMargin: 0,
				borderWidth: {top: 1, bottom:0, right:1,left:1},
				borderColor: "#eee",
				minBorderMargin: 5,
				clickable: true,
				hoverable: true,
				autoHighlight: true,
				mouseActiveRadius: 10
			},
			series: {
				lines: {
					show: true,
					fill: true,
					lineWidth: 2,
					steps: false
				},
				points: {
					show: true,
					radius: 4.5,
					symbol: "circle",
					lineWidth: 3.0
				}
			}
			,
			legend: {
				position: "nw",
				margin: [0, -25],
				noColumns: 0,
				labelBoxBorderColor: null,
				labelFormatter: function(label, series) {
					return label + '&nbsp;&nbsp;';
				},
				width: 40,
				height: 1
			},
			colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
			shadowSize: 0,
			tooltip: true,
			tooltipOpts: {
				content: "%s: %y.0",
				xDateFormat: "%d/%m",
			shifts: {
				x: -30,
				y: -50
			},
			defaultTheme: false
			},
			yaxis: {
				min: min_price
			}
			,
			xaxis: {
				mode: "time",
				minTickSize: [1, "day"],
				timeformat: "%d/%m/%y"
			}
		};	
		

	$.plot( $("#chart_plot_b2w"), 
		// [{ 
			// label: lojaArr[0], 
			// data: newArr,
			// lines: { 
				// fillColor: "rgba(150, 202, 89, 0.12)" 
			// }, 
			// points: { 
				// fillColor: "#fff" } 
		// }]
		chartinfo
		, chart_plot_02_settings);
		
	var previousPoint = null;
    $("#chart_plot_b2w").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
		//console.log(item);
                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                    //console.log(y);
					y = (Number(y)).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
		//console.log(y);
                showTooltip(item.pageX, item.pageY, "(" + y + ")");
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
		
		
	function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #e7e7e7',
			color: '#000',
            padding: '5px',
            'background-color': '#F7F7F7'
        }).appendTo("body").fadeIn(200);
    }
}