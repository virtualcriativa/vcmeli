$(document).ready(function(){
    console.log(store);
    console.log(BASE_URL);
    console.log(SKUB2W);
    function get_product_details()
    {
        var produto = null;
        console.log(BASE_URL+'app/product/get_details/'+store+'/'+SKUB2W);
        var request = $.ajax({ 
            type : "GET",
            async: false,
            url: BASE_URL+'app/product/get_details/'+store+'/'+SKUB2W+'/json/', 
            cache : false
        });
        request.always(function(res)
        {
            produto = JSON.parse(res);
        });
        return produto;
    }

    var details = get_product_details();
    //console.log(details);

    $(".select-frete").change(function(){
        $('.frete tbody').html('');
        var estado = $(this).val().toLowerCase();
        var lista_cep = get_region_zip_code(estado);

        $("#money").inputmask('decimal', {
            'alias': 'numeric',
            'groupSeparator': '.',
            'autoGroup': true,
            'digits': 2,
            'radixPoint': ",",
            'digitsOptional': false,
            'allowMinus': false,
            'prefix': 'R$ ',
            'placeholder': ''
        });

/*         var lista_cep = [
        '03182050', //São Paulo - Capital
        '18085844', //São Paulo - Interior
        '31270901', //Minas Gerais - Capital
        '38402100', //Minas Gerais - Interior
        '20011901', //Rio de Janeiro - Capital
        '23017410', //Rio de Janeiro - Interior
        '29045402', //Espírito Santo - Capital
        '29190010', //Espírito Santo - Interior
        ]; */

        var lista_cep_cidades = [
        'Capital',
        'Interior',
        ];

        for(var i=0; i<lista_cep.length; i++){

            var html = "<tr><td colspan=6 style='background-color: #f5f7fa;text-align: center;font-size: 2rem;'><strong>" + lista_cep_cidades[i] + "</strong></tr>";
            $('.frete tbody').append(html);
            for(var x=0; x<details.sku_competitors.length; x++){
                console.log(details.sku_competitors[x]);
                id_loja         = details.sku_competitors[x].id_loja;
                var nome_loja   = details.sku_competitors[x].loja;
                var request = $.ajax({ 
                    type : "GET",
                    async: false,
                    url: 'https://freight-v3-americanas.b2w.io/freight?c_b2wPid=1495123057747.0.04023890091103555&c_prime=false&c_salesSolution=&cep='+lista_cep[i]+'&opn=AFLACOM&product={"sku":"'+details.sku.skub2w+'","promotionedPrice":26.89,"condition":"NEW","quantity":1,"storeId":"'+id_loja+'"}', 
                    cache : false
                });
      
                request.done(function(res)
                {
                  for(var y=0; y<res.options.length; y++){
                    
                    var preco_frete = res.options[y].price;
                    var tipo_frete  = res.options[y].id;
                    tipo_frete      = get_freight_name(tipo_frete);
                    carrinho = parseFloat(details.sku_competitors[x].preco) + parseFloat(preco_frete);
                    carrinho = "R$"+numeroParaMoeda(carrinho);
                    if(preco_frete == "0"){
                      preco_frete = "Grátis";
                    }else{
                      preco_frete = "R$"+numeroParaMoeda(preco_frete);
                    }
                    console.log(carrinho);
                    var html = "<tr><td>"+nome_loja+"</td><td>"+tipo_frete+"</td><td>"+preco_frete+"</td><td>"+res.options[y].eta.lowerBoundBusinessDays+" dias(s)</td><td>"+res.options[y].eta.businessDays+" dias(s)</td><td>"+carrinho+"</td></tr>";
                    $('.frete tbody').append(html);
                  }
                });
                
                request.fail(function(res)
                {
                  var tipo_frete  = res.responseJSON.message;
                  tipo_frete      = get_freight_name(tipo_frete);
                  var markup = "<tr><td>"+nome_loja+"</td><td colspan='5'>"+tipo_frete+"</td></tr>";
                  $('.frete').append(markup);
                });
            }
            
        }


    });


    function get_freight_name(tipo_frete){
      switch(tipo_frete) {
          case 'CONVENTIONAL':
            return "Convencional";
            break;
          case 'CONVENTIONALPRIME':
            return "Prime";
            break;
          case 'Produtos não entregues em sua região.':
            return "Região não atendida.";
            break;
          default:
            return tipo_frete;
      }
    }

    function get_region_zip_code(region)
    {
        switch(region){
            case "sp":
            return lista_cep = [
                '03182050', //São Paulo - Capital
                '18085844', //São Paulo - Interior
            ];
            break;

            case "mg":
            return lista_cep_mg = [
                '31270901', //Minas Gerais - Capital
                '38402100', //Minas Gerais - Interior
            ];
            break;

            case "rj":
            return lista_cep_rj = [
                '20011901', //Rio de Janeiro - Capital
                '23017410', //Rio de Janeiro - Interior
            ];
            break;

            case "es":
            return lista_cep_es = [
                '29045402', //Espírito Santo - Capital
                '29190010', //Espírito Santo - Interior
            ];
            break;
        }

    }
    });