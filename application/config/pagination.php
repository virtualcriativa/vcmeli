<?php
$config['reuse_query_string'] = true;
$config['first_link'] = 'Primeira';
$config['last_link'] = 'Última';
$config['full_tag_open'] = '<div class="pagination pagination-split">';
$config['full_tag_close'] = '</div>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li><a>';
$config['cur_tag_close'] = '</a></li>';
$config['next_link'] = false;
$config['prev_link'] = false;
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
