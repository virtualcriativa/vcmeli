
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mercado Livre Gestão Inteligente | VC Meli</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php
		include_once "templates/left_nav.php";
		?>
        <!-- top navigation -->
        <?php
		include_once "templates/top_nav.php";
		?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
		  <div class="row">

		  <?php if(isset($_SESSION["message"])): ?>
				<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="alert alert-warning" role="alert">
					<?php echo $_SESSION["message"];?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		
		<?php
		$fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::CURRENCY);
		$pct = new \NumberFormatter( 'pt_BR', \NumberFormatter::PERCENT);
		$pct->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 2);
		?>
		  <div class="row">
			<div class="tile_count">
				
				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total de Produtos</span>
				<div class="count"><?php echo $anuncios_total; ?></div>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-clock-o"></i> Total de Vendas</span>
				<div class="count"><?php echo $estatisticas->seller_reputation->transactions->total; ?></div>
				<span class="count_bottom">Em todo o período</span>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total de Vendas</span>
				<div class="count"><?php echo $estatisticas->seller_reputation->metrics->sales->completed; ?></div>
				<span class="count_bottom">últimos 60 dias</span>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Avaliações Positivas</span>
				<div class="count"><?php echo $estatisticas->seller_reputation->transactions->ratings->positive* 100; ?>%</div>
				<span class="count_bottom"><?php echo $estatisticas->seller_reputation->transactions->ratings->negative * 100; ?>% negativas e <?php echo $estatisticas->seller_reputation->transactions->ratings->neutral * 100; ?>% Neutras</span>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="tile_count">
				
				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Faturamento</span>
				<div class="count"><?php echo $fmt->format((string)$venda_mensal); ?></div>
				<span class="count_bottom">Mês atual</span>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-clock-o"></i> Custo De Produto</span>
				<div class="count"><?php echo $fmt->format((string)$custo_mensal); ?></div>
				<span class="count_bottom">Custo do bling</span>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total de Comissão</span>
				<div class="count"><?php echo $fmt->format((string)$comissao_mensal); ?></div>
				<span class="count_bottom">Mês atual</span>
				</div>

				<div class="col-md-3 col-sm-3  tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Taxa de Conversão</span>
				<div class="count"><?php echo $pct->format($total_sales_number/$total_visits); ?></div>
				<span class="count_bottom">Vendas / Vistas do mês atual</span>
				</div>

			</div>
		</div>

            <div class="row top_tiles">

			<?php if(!isset($_SESSION["meli_user"]["meli_user_id"])): ?>
				<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="alert alert-danger" role="alert">
					Você ainda não possui nenhuma conta do Mercado Livre vinculada.
					Clique aqui para vincular sua primeira conta.
					</div>
				</div>
			<?php endif; ?>
            
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
				<div class="icon"><i class="fa fa-tags"></i></div>
                  <div class="count">Mais Vendidos</div>
                  <h3>Por quantidade</h3>

				  <table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Item</th>
							<th># vendas</th>
						</tr>
					</thead>
					<tbody>
				  <?php foreach($mais_vendidos as $mais_vendido): ?>			
					<tr>
						<td><img src="<?php echo $mais_vendido->thumbnail; ?>" width=30 /></td>
						<td><small><?php echo $mais_vendido->title; ?></small></td>
						<td class="d-none d-md-table-cell"><small><?php echo $mais_vendido->sold_quantity; ?></small></td>
					</tr>
				  <?php endforeach; ?>
					</tbody>
				</table>
				</div>
			  </div>
			  
			  
			  <div class="animated flipInY col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="tile-stats">
				<div class="icon"><i class="fa fa-tags"></i></div>
                  <div class="count">Mais Vendidos</div>
                  <h3>Por valor vendido</h3>

				  <table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Item</th>
							<th># vendas</th>
						</tr>
					</thead>
					<tbody>
				  <?php foreach($mais_vendidos as $mais_vendido): ?>			
					<tr>
						<td><img src="<?php echo $mais_vendido->thumbnail; ?>" width=30 /></td>
						<td><small><?php echo $mais_vendido->title; ?></small></td>
						<td class="d-none d-md-table-cell"><small><?php echo $mais_vendido->sold_quantity; ?></small></td>
					</tr>
				  <?php endforeach; ?>
					</tbody>
				</table>
				</div>
			  </div>

              

            </div> <!-- <-- fecha row top_tiles -->
			


        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php
		include_once "templates/footer.php";
		?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/echarts.min.js"></script>
    <script>
    $( document ).ready(function() {

      var theme = {
				  color: [
					  '#c13838', '#26B99A', '#3498DB', '#34495E',
					  '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
				  ],

				  title: {
					  itemGap: 8,
					  textStyle: {
						  fontWeight: 'normal',
						  color: '#408829'
					  }
				  },

				  dataRange: {
					  color: ['#1f610a', '#97b58d']
				  },

				  toolbox: {
					  color: ['#408829', '#408829', '#408829', '#408829']
				  },

				  tooltip: {
					  backgroundColor: 'rgba(0,0,0,0.5)',
					  axisPointer: {
						  type: 'line',
						  lineStyle: {
							  color: '#408829',
							  type: 'dashed'
						  },
						  crossStyle: {
							  color: '#408829'
						  },
						  shadowStyle: {
							  color: 'rgba(200,200,200,0.3)'
						  }
					  }
				  },

				  dataZoom: {
					  dataBackgroundColor: '#eee',
					  fillerColor: 'rgba(64,136,41,0.2)',
					  handleColor: '#408829'
				  },
				  grid: {
					  borderWidth: 0
				  },

				  categoryAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },

				  valueAxis: {
					  axisLine: {
						  lineStyle: {
							  color: '#408829'
						  }
					  },
					  splitArea: {
						  show: true,
						  areaStyle: {
							  color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
						  }
					  },
					  splitLine: {
						  lineStyle: {
							  color: ['#eee']
						  }
					  }
				  },
				  timeline: {
					  lineStyle: {
						  color: '#408829'
					  },
					  controlStyle: {
						  normal: {color: '#408829'},
						  emphasis: {color: '#408829'}
					  }
				  },

				  k: {
					  itemStyle: {
						  normal: {
							  color: '#68a54a',
							  color0: '#a9cba2',
							  lineStyle: {
								  width: 1,
								  color: '#408829',
								  color0: '#86b379'
							  }
						  }
					  }
				  },
				  map: {
					  itemStyle: {
						  normal: {
							  areaStyle: {
								  color: '#ddd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  },
						  emphasis: {
							  areaStyle: {
								  color: '#99d2dd'
							  },
							  label: {
								  textStyle: {
									  color: '#c12e34'
								  }
							  }
						  }
					  }
				  },
				  force: {
					  itemStyle: {
						  normal: {
							  linkStyle: {
								  strokeColor: '#408829'
							  }
						  }
					  }
				  },
				  chord: {
					  padding: 4,
					  itemStyle: {
						  normal: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  },
						  emphasis: {
							  lineStyle: {
								  width: 1,
								  color: 'rgba(128, 128, 128, 0.5)'
							  },
							  chordStyle: {
								  lineStyle: {
									  width: 1,
									  color: 'rgba(128, 128, 128, 0.5)'
								  }
							  }
						  }
					  }
				  },
				  gauge: {
					  startAngle: 225,
					  endAngle: -45,
					  axisLine: {
						  show: true,
						  lineStyle: {
							  color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
							  width: 8
						  }
					  },
					  axisTick: {
						  splitNumber: 10,
						  length: 12,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  axisLabel: {
						  textStyle: {
							  color: 'auto'
						  }
					  },
					  splitLine: {
						  length: 18,
						  lineStyle: {
							  color: 'auto'
						  }
					  },
					  pointer: {
						  length: '90%',
						  color: 'auto'
					  },
					  title: {
						  textStyle: {
							  color: '#333'
						  }
					  },
					  detail: {
						  textStyle: {
							  color: 'auto'
						  }
					  }
				  },
				  textStyle: {
					  fontFamily: 'Arial, Verdana, sans-serif'
				  }
        };
        

    	if ($('#echart_pie').length ){  
			  
			  var echartPie = echarts.init(document.getElementById('echart_pie'), theme);

			  echartPie.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['68%Fora do Buy Box #1', '2%Ganhando #2', '15%Exclusivo #3', '15%Sem estoque #4']
				},
				toolbox: {
				  show: true,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel'],
					  option: {
						funnel: {
						  x: '25%',
						  width: '50%',
						  funnelAlign: 'left',
						  max: 1548
						}
					  }
					},
					restore: {
					  show: false,
					  title: "Restore"
					},
					saveAsImage: {
					  show: false,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				series: [{
				  name: 'Americanas',
				  type: 'pie',
				  radius: '55%',
				  center: ['50%', '48%'],
				  data: [{
					value: <?php echo $c_americanas_lose;?>,
					name: 'Fora do BuyBox'
				  }, {
					value: <?php echo $c_americanas;?>,
					name: 'Ganhando'
				  }, {
					value: <?php echo $c_americanas_exclusive;?>,
					name: 'Exclusivo'
				  }, {
					value: <?php echo $c_americanas_out;?>,
					name: 'Sem estoque'
				  }]
				}]
			  });

			  var dataStyle = {
				normal: {
				  label: {
					show: false
				  },
				  labelLine: {
					show: false
				  }
				}
			  };

			  var placeHolderStyle = {
				normal: {
				  color: 'rgba(0,0,0,0)',
				  label: {
					show: false
				  },
				  labelLine: {
					show: true
				  }
				},
				emphasis: {
				  color: 'rgba(0,0,0,0)'
				}
			  };

      }  
      

      if ($('#echart_pie2').length ){  
			  
			  var echartPie = echarts.init(document.getElementById('echart_pie2'), theme);

			  echartPie.setOption({
				tooltip: {
				  trigger: 'item',
				  formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
				  x: 'center',
				  y: 'bottom',
				  data: ['Fora do Buy Box', '2%Ganhando #1', '15%Exclusivo #3', '15%Sem estoque #4']
				},
				toolbox: {
				  show: false,
				  feature: {
					magicType: {
					  show: true,
					  type: ['pie', 'funnel'],
					  option: {
						funnel: {
						  x: '25%',
						  width: '50%',
						  funnelAlign: 'left',
						  max: 1548
						}
					  }
					},
					restore: {
					  show: false,
					  title: "Restore"
					},
					saveAsImage: {
					  show: false,
					  title: "Save Image"
					}
				  }
				},
				calculable: true,
				series: [{
				  name: 'Submarino',
				  type: 'pie',
				  radius: '55%',
				  center: ['50%', '48%'],
				  data: [{
					value: <?php echo $c_submarino_lose;?>,
					name: 'Fora do BuyBox'
				  }, {
					value: <?php echo $c_submarino;?>,
					name: 'Ganhando'
				  }, {
					value: <?php echo $c_submarino_exclusive;?>,
					name: 'Exclusivo'
				  }, {
					value: <?php echo $c_submarino_out;?>,
					name: 'Sem estoque'
				  }]
				}]
			  });

			  var dataStyle = {
				normal: {
				  label: {
					show: true
				  },
				  labelLine: {
					show: false
				  }
				}
			  };

			  var placeHolderStyle = {
				normal: {
				  color: 'rgba(0,0,0,0)',
				  label: {
					show: true
				  },
				  labelLine: {
					show: false
				  }
				},
				emphasis: {
				  color: 'rgba(0,0,0,0)'
				}
			  };

			} 

      
    });
      </script>

    
  </body>
</html>