<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Perguntas</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- iCheck -->
    <link href="<?php echo base_url();?>/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
	<script src="https://kit.fontawesome.com/3567375b2c.js" crossorigin="anonymous"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <?php
        include_once dirname(__FILE__) . "/../templates/left_nav.php";
        ?>
        <!-- top navigation -->
        <?php
        include_once dirname(__FILE__) . "/../templates/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-12 col-xl-6">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Perguntas</h2>
                    
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                  <?php if(isset($message)): ?>
                    <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="alert alert-warning" role="alert">
                      <?php echo $message;?>
                      </div>
                    </div>
                  <?php endif; ?>

                   <?php if(!count($questions)): ?>
                    
                    <div><strong>Você não tem perguntas para serem respondidas.</strong></div>
                   <?php else: ?>
                    <p>Você tem um total de <?php echo count($questions); ?> perguntas</p>

                      <?php foreach($questions as $question): ?>
                      <?php //print_r($question); die;?>

                      <div class="row">
                        <div class="col-12 col-xl-6">
                          <div class="x_panel">

                          <?php 
                              $date = new DateTime($question->date_created);
                            ?>
                          <div class="x_title">



                            <div class="row">
                              <div class="col-md-2 col-sm-3">
                                <img src="<?php echo $question->item->secure_thumbnail; ?>" />
                              </div>
                              <div class="col-md-10 col-sm-9">
                              <h4><?php echo $question->item->title; ?></h4>
                              <?php
                              $fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::CURRENCY );
                              echo $fmt->format((string)$question->item->price); ?>
                              </div>
                            </div>


                            
                            <h2><?php echo $question->text; ?></h2>
                            <div class="clearfix"></div>
                            <div class="form-group row">
                              <form action="../questions/send_answer" accept-charset="UTF-8" method="post" name="<?php echo $question->id; ?>" class="form-horizontal form-label-left">
                                
                                  <div class="item form-group">
                                    <div class="col-md-12 col-sm-12">
                                      <textarea name="text" class="form-control" rows="3" placeholder="Sua resposta" style="margin-top: 0px; margin-bottom: 0px; height: 111px;"></textarea>
                                    </div>
                                  </div>
                                  <div class="ln_solid"></div>
                                  <div class="item form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <input type="hidden" name="question_id" value="<?php echo $question->id; ?>" />
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                              </form>
                            </div>

                          </div>

                          </div>    
                        </div>
                      </div>

                      <?php endforeach; ?>

                    <?php endif; ?>
                    
                  </div>
                </div>

                </div>
            </div>

        </div>
        <!-- /page content -->

        <?php
        include_once dirname(__FILE__) . "/../templates/footer.php";
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
  
	  <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/echarts.min.js"></script>
	<!-- <script type="text/javascript">
	var BASE_URL = "<?php echo base_url();?>";
	</script>
	<script src="<?php echo base_url();?>assets/js/lista_anuncios.js"></script> -->

    
  </body>
</html>