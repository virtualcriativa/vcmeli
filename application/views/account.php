<!DOCTYPE html>

<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Precificação Inteligente | </title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>/assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <link href="<?php echo base_url();?>/assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php
		include_once "templates/left_nav_planos.php";
		?>
        <!-- top navigation -->
        <?php
		include_once "templates/top_nav.php";
		?>
        <!-- /top navigation -->
	
        <!-- page content -->
    <div class="right_col" role="main">
      <div class="">
          <div class="row">
            <?php if(isset($_SESSION["message"])): ?>
              <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-warning" role="alert">
                <?php echo $_SESSION["message"];?>
                </div>
              </div>
            <?php endif; ?>
          </div>
        
        <div class="row">
        <div class="col-md-6 col-xs-12 widget_tally_box">
            <div class="x_panel ui-ribbon-container ">
            <div class="x_title">
            <h2>Meus Dados</h2>
            <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <section class="login_content">
                    <form name="user_account" method="post" action="<?php echo base_url();?>user/update"> 
                        <div class="col-md-12 col-xs-12">
                            <input type="text" class="form-control" name="first_name" required value="<?php echo $first_name; ?>" />
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <input type="text" class="form-control" name="last_name" required value="<?php echo $last_name; ?>" />
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <input type="text" class="form-control" name="email" required value="<?php echo $email; ?>" />
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <input type="text" class="form-control" name="phone" required value="<?php echo $phone; ?>" />
                        </div>
                        
                        <div class="col-md-2 col-xs-2">
                        	<label for="password">Senha</label>
                        </div>
                        <div class="col-md-10 col-xs-10">
                            <input type="password" class="form-control" name="password" value="" />
                        </div>
                        <div class="col-md-8 col-xs-12">
                                <input type="text" name="endereco" class="form-control" placeholder="Endereço" value="<?php echo isset($endereco)?$endereco:""; ?>" />
                              </div>
                              <div class="col-md-2 col-xs-12">
                                  <input type="text" name="numero" class="form-control" placeholder="Núm" value="<?php echo isset($numero)?$numero:""; ?>" />
                                </div>
                              <div class="col-md-2 col-xs-12">
                                <input type="text" name="complemento" class="form-control" placeholder="Compl" value="<?php echo isset($complemento)?$complemento:""; ?>" />
                              </div>
                              <div class="col-md-3 col-xs-12">
                                  <input type="text" name="bairro" class="form-control" placeholder="Bairro" value="<?php echo isset($bairro)?$bairro:""; ?>" />
                              </div>
                              <div class="col-md-3 col-xs-12">
                                  <input type="text" name="cidade" class="form-control" placeholder="Cidade" value="<?php echo isset($cidade)?$cidade:""; ?>" />
                              </div>
                              
                              <div class="col-md-3 col-xs-12">
                                  <select name="estado" class="form-control">
                                      <option value="AC" <?php echo (isset($estado) && $estado=='AC')?'selected':''; ?>>Acre</option>
                                      <option value="AL" <?php echo (isset($estado) && $estado=='AL')?'selected':''; ?>>Alagoas</option>
                                      <option value="AP" <?php echo (isset($estado) && $estado=='AP')?'selected':''; ?>>Amapá</option>
                                      <option value="AM" <?php echo (isset($estado) && $estado=='AM')?'selected':''; ?>>Amazonas</option>
                                      <option value="BA" <?php echo (isset($estado) && $estado=='BA')?'selected':''; ?>>Bahia</option>
                                      <option value="CE" <?php echo (isset($estado) && $estado=='CE')?'selected':''; ?>>Ceará</option>
                                      <option value="DF" <?php echo (isset($estado) && $estado=='DF')?'selected':''; ?>>Distrito Federal</option>
                                      <option value="ES" <?php echo (isset($estado) && $estado=='ES')?'selected':''; ?>>Espírito Santo</option>
                                      <option value="GO" <?php echo (isset($estado) && $estado=='GO')?'selected':''; ?>>Goiás</option>
                                      <option value="MA" <?php echo (isset($estado) && $estado=='MA')?'selected':''; ?>>Maranhão</option>
                                      <option value="MT" <?php echo (isset($estado) && $estado=='MT')?'selected':''; ?>>Mato Grosso</option>
                                      <option value="MS" <?php echo (isset($estado) && $estado=='MS')?'selected':''; ?>>Mato Grosso do Sul</option>
                                      <option value="MG" <?php echo (isset($estado) && $estado=='MG')?'selected':''; ?>>Minas Gerais</option>
                                      <option value="PA" <?php echo (isset($estado) && $estado=='PA')?'selected':''; ?>>Pará</option>
                                      <option value="PB" <?php echo (isset($estado) && $estado=='PB')?'selected':''; ?>>Paraíba</option>
                                      <option value="PR" <?php echo (isset($estado) && $estado=='PR')?'selected':''; ?>>Paraná</option>
                                      <option value="PE" <?php echo (isset($estado) && $estado=='PE')?'selected':''; ?>>Pernambuco</option>
                                      <option value="PI" <?php echo (isset($estado) && $estado=='PI')?'selected':''; ?>>Piauí</option>
                                      <option value="RJ" <?php echo (isset($estado) && $estado=='RJ')?'selected':''; ?>>Rio de Janeiro</option>
                                      <option value="RN" <?php echo (isset($estado) && $estado=='RN')?'selected':''; ?>>Rio Grande do Norte</option>
                                      <option value="RS" <?php echo (isset($estado) && $estado=='RS')?'selected':''; ?>>Rio Grande do Sul</option>
                                      <option value="RO" <?php echo (isset($estado) && $estado=='RO')?'selected':''; ?>>Rondônia</option>
                                      <option value="RR" <?php echo (isset($estado) && $estado=='RR')?'selected':''; ?>>Roraima</option>
                                      <option value="SC" <?php echo (isset($estado) && $estado=='SC')?'selected':''; ?>>Santa Catarina</option>
                                      <option value="SP" <?php echo (isset($estado) && $estado=='SP')?'selected':''; ?>>São Paulo</option>
                                      <option value="SE" <?php echo (isset($estado) && $estado=='SE')?'selected':''; ?>>Sergipe</option>
                                      <option value="TO" <?php echo (isset($estado) && $estado=='TO')?'selected':''; ?>>Tocantins</option>
                                    </select>
                              </div>
            
                              <div class="col-md-3 col-xs-12">
                                  <input type="text" name="cep" class="form-control" placeholder="CEP" data-inputmask="'mask' : '99999-999'" value="<?php echo (isset($cep))?$cep:""; ?>" />
                              </div>
                              
                              
                              <div class="col-md-12 col-xs-12">
                                <button type="submit" class="btn btn-primary save_payment" name="save_payment">Salvar</button>
                            </div>
                            <input type="hidden" value="<?php echo $id; ?>" name="id_usuario" class="id_usuario" />

                    </form>
                </section>
            </div>
            </div>
        </div>

        </div>

    </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php
		include_once "templates/footer.php";
		?>
        <!-- /footer content -->
      </div>
    </div>
  </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>

    <script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>


    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    
    <script src="<?php echo base_url();?>assets/vendors/switchery/dist/switchery.min.js"></script>

    <script src="<?php echo base_url();?>assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>

  </body>
</html>