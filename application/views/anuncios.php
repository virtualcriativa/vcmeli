<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Anúncios</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- iCheck -->
    <link href="<?php echo base_url();?>/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
	<script src="https://kit.fontawesome.com/3567375b2c.js" crossorigin="anonymous"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php
        include_once "templates/left_nav.php";
        ?>
        <!-- top navigation -->
        <?php
        include_once "templates/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                <div class="col-12 col-xl-6">

                <div class="x_panel">
                  <div class="x_title">
                    <h2>Anúncios</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <?php
                  include_once "templates/listings_filter.php";
                  ?>

                   <?php if(!count($anuncios)): ?>
                    <div><strong>Você ainda não tem anúncios nessa conta.</strong></div>
                   <?php else: ?>
                    <p>Você tem um total de <?php echo $anuncios_total; ?> anúncios</p>
                    <!-- start project list -->
                    <table id="" class="table table-striped projects" style="width:100%">
                      <thead>
                        <tr>
                          <th>Imagem</th>
                          <th>Anúncio</th>
                          <th>Qualidade</th>
                          <th>Info</th>
                          <th style="width: 20%">Edit</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      <?php foreach($anuncios as $anuncio): ?>
                      <?php 
                      //print_r($anuncio);
                      //$visitas = $anuncio["visitas"];
                      $anuncio = $anuncio->body;
                      ?>
                        <tr>
                          <td>
                            <img src="<?php echo($anuncio->secure_thumbnail); ?>" width="70" />
                          </td>
                          <td>
                          <small><?php echo constant($anuncio->status); ?></small> 
                          <br> 
                          <?php echo $anuncio->title; ?> <a href="<?php echo $anuncio->permalink;?>" target="_blank"><i class="fa fa-link"></i></a>
                            <br>
                            <small>Sku: <?php echo $anuncio->seller_custom_field;?> | Anúncio: <?php echo constant($anuncio->listing_type_id);?></small>
                            <p><strong>
                            <?php 
                            $fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::CURRENCY );
                            echo $fmt->format((string)$anuncio->price); 
                            ?>
                            </strong></p>
                          </td>
                          <td class="project_progress">
                            <?php 
                            $fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::PERCENT );
                            echo $fmt->format($anuncio->health);
                            
                            if($fmt->format($anuncio->health) <= 50){
                              $css_class = "bg-red";
                              $quality = "Precisa melhorar";
                            }elseif($fmt->format($anuncio->health) >= 90){
                              $css_class = "bg-green";
                              $quality = "Excelente!";
                            }else{
                              $css_class = "bg-orange";
                              $quality = "Satisfatório";
                            }

                            echo " <small>".$quality."</small>"; 
                            ?>
                            <div class="progress progress_sm">
                              <div class="progress-bar <?echo $css_class;?>" role="progressbar" data-transitiongoal="<?php echo $fmt->format($anuncio->health);?>" aria-valuenow="56" style="width: <?php echo $fmt->format($anuncio->health);?>;"></div>
                            </div>
                          </td>

                          <td class="project_progress">
                          <?php $visitas = array_shift($anuncio->visitas); ?>
                            <small><i class="fa fa-shopping-cart"></i></small> <?php echo $anuncio->sold_quantity;?> vendas<br>
                            <small><i class="fa fa-th-large"></i></small> <?php echo $anuncio->available_quantity;?> em estoque<br>
                            <small><i class="fa fa-chart-bar"></i></small> <?php echo $visitas;?> visita(s)<br>
                            <?php $perc =  ((int)$visitas >0)?(int)$anuncio->sold_quantity/(int)$visitas*100:0;?>
                            <small><i class="fas fa-chart-pie"></i></small> <?php echo number_format($perc, 2, '.', ',');?> %
                          </td>
                          <td>
                           <!-- <a href="<?php echo base_url();?>anuncios/edit/<?php echo $anuncio->id;?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> Editar </a>-->
                            <!--<a href="#" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Duplicar </a>-->
                            <a href="<?php echo base_url();?>anuncios/performance/<?php echo $anuncio->id;?>" class="btn btn-success btn-xs"><i class="fa fa-trophy"></i> Stats </a>
                            <!--<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>-->
                          </td>
                        </tr>
                      <?php endforeach; ?>
                        
                      </tbody>
                    </table>
                    <!-- end project list -->
                    <?php echo $paginacao; ?>
                    <?php endif; ?>
                  </div>
                </div>

                </div>
            </div>

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php
        include_once "templates/footer.php";
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
  
	  <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/echarts.min.js"></script>
	<!-- <script type="text/javascript">
	var BASE_URL = "<?php echo base_url();?>";
	</script>
	<script src="<?php echo base_url();?>assets/js/lista_anuncios.js"></script> -->
    
  </body>
</html>