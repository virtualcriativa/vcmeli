<?php //print_r(($anuncio)); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Performance de Anúncio</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Switchery -->
    <link href="<?php echo base_url();?>/assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  
    <!-- iCheck -->
    <link href="<?php echo base_url();?>/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">

    <link href="<?php echo base_url();?>/assets/css/anuncios.css" rel="stylesheet">


    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
	<script src="https://kit.fontawesome.com/3567375b2c.js" crossorigin="anonymous"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php
        include_once dirname(__FILE__) . "/../templates/left_nav.php";
        ?>
        <!-- top navigation -->
        <?php
        include_once dirname(__FILE__) . "/../templates/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Detalhes do Anúncio</h3>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h5><?php
                    foreach($category_details["path_from_root"] as $key => $category){
                      echo $category->name ." > ";
                      $category_name = $category->name;
                    }
                    
                    ?></h5>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-3 col-sm-12 ">
                      <div class="product-image text-center">
                        <img src="<?php echo $anuncio["pictures"][0]->secure_url; ?>" style="width: auto; max-height: 220px" />
                      </div>
                    </div>

                    <div class="col-md-9 col-sm-7 " style="border:0px solid #e5e5e5;">
                    
                      
                      <h3 class="prod_title"><?php echo $anuncio["title"]; ?> <br><small>(<?php echo $anuncio["id"];?>)</small></h3> 
                      <ul class="stats-overview">
                        <li>
                        <span class="name"> <i class="fa fa-shopping-cart"></i> Vendas </span>
                        <span class="value text-success"> <?php echo $anuncio["sold_quantity"];?> </span>
                        </li>
                        <li>
                        <span class="name"> <i class="fa fa-th-large"></i> Estoque atual </span>
                        <span class="value text-success"> <?php echo $anuncio["available_quantity"];?> </span>
                        </li>
                        <li>
                        <span class="name"> <i class="fa fa-chart-bar"></i> Visitas </span>
                        <?php
                        $visitas = (array)$visitas;
                        $visitas = array_shift($visitas);
                        ?>
                        <span class="value text-success"> <?php echo $visitas;?> </span>
                        </li>
                      </ul>
                      <ul class="stats-overview">
                        <li>
                        <span class="name"> <i class="fas fa-retweet"></i> Conversão </span>
                        <?php $perc =  ((int)$visitas >0)?(int)$anuncio["sold_quantity"]/(int)$visitas*100:0;?>
                        <span class="value text-success"> <?php echo number_format($perc, 2, '.', ',');?> % </span>
                        </li>
                        <li>
                        <span class="name"> <i class="fas fa-unsorted"></i> Posicionamento </span>
                        <span class="value text-success"> <?php echo $listing_position;?> </span>
                        </li>
                        <li>
                        <span class="name"> <i class="fas fa-money"></i> Preço </span>
                        <?php
                        $fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::CURRENCY );
                        ?>
                        <span class="value text-success"> <?php echo $fmt->format((string)$anuncio["price"]); ?> </span>
                        </li>
                      </ul>

                      

                    </div>

                  </div>
                </div>
              </div>
            </div>


            <?php if(count($best_sellers)): ?>
            <div class="row">
              <div class="col-md-12 col-sm-12 " style="overflow-x: scroll; height: 300px;">
                <div class="x_panel">
                  <div class="x_title">
                    <h5>Mais vendidos da categoria</h5>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <?php foreach($best_sellers as $key=>$best_sellers): ?>
                    <?php $best_sellers = (array)$best_sellers->body;?>
                    <div class="col-md-4 text-center" style=" margin-bottom: 20px">
                    
                      <img src="<?php echo $best_sellers["secure_thumbnail"]; ?>" />
                      <p style="min-height: 60px"><a href="<?php echo $best_sellers["permalink"]; ?>" target="_blank"> <?php echo $best_sellers["title"]; ?> <br><small>(<?php echo $best_sellers["id"];?>)</small></a></p> 

                        <ul class="stats-overview">
                          <li>
                          <span class="name"> <i class="fas fa-money"></i> Preço </span>
                          <?php
                          $fmt = new \NumberFormatter( 'pt_BR', \NumberFormatter::CURRENCY );
                          ?>
                          <span class="value text-success"> <?php echo $fmt->format((string)$best_sellers["price"]); ?> </span>
                          </li>
                          <li>
                          <span class="name"> <i class="fa fa-shopping-cart"></i> Vendas </span>
                          <span class="value text-success"> <?php echo $best_sellers["sold_quantity"];?> </span>
                          </li>
                        </ul>
                      
                    </div>
                    <?php endforeach; ?>

                  </div>
                </div>
              </div>
            </div>
            <?php endif; ?>
          </div>
        </div>
        <!-- /page content -->

        

        <!-- STATS -->
        <div class="right_col" role="" style="min-height: auto !important">
          <div class="row">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Estatísticas do Anúncio</h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                <div class="well" style="overflow: auto">

                    <div class="col-md-8">
                    <p>Selecione o intervalo de data</p>
                    </div>
                    <div class="col-md-4">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span></span> <b class="caret"></b>
                    </div>
                    </div>
                </div>


                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Vendas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Visitas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Conversão</a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    
                                <div class="row">
                                  <div class="tile_count">
                                    <div class="col-md-3 col-sm-4  tile_stats_count">
                                      <span class="count_top"><i class="fa fa-dashboard"></i> Total Vendido</span>
                                      <div class="count count_sales"></div>
                                      <span class="count_bottom"> No período</span>
                                    </div>
                                    <div class="col-md-3 col-sm-4  tile_stats_count">
                                      <span class="count_top"><i class="fa fa-clock-o"></i> Média de</span>
                                      <div class="count count_sales_average"></div>
                                      <span class="count_bottom">Vendas por dia</span>
                                    </div>
                                    <div class="col-md-3 col-sm-4  tile_stats_count">
                                      <span class="count_top"><i class="fa fa-money"></i> Faturamento</span>
                                      <div class="count count_renevue"></div>
                                      <span class="count_bottom"> Desse anúncio</span>
                                    </div>
                                    <div class="col-md-3 col-sm-4  tile_stats_count">
                                      <span class="count_top"><i class="fa fa-shopping-cart"></i> Comissão Paga</span>
                                      <div class="count count_fee"></div>
                                      <span class="count_bottom">No período</span>
                                    </div>
                                    
                                  </div>
                                </div>
                  
                  
                  <div style="height: 250px">  
                        <canvas id="salesLineChart"></canvas>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div style="height: 250px">  
                      <canvas id="visitsLineChart"></canvas>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div style="height: 250px">  
                        <canvas id="conversionLineChart"></canvas>
                      </div>
                    </div>
                  </div>
                </div>
                  
                </div>
              </div>
            </div>
        </div>
        <!-- / STATS -->

        <!-- footer content -->
        <?php
        include_once dirname(__FILE__) . "/../templates/footer.php";
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Switchery -->
    <script src="<?php echo base_url();?>assets/vendors/switchery/dist/switchery.min.js"></script>
  
    <!-- jQuery Tags Input -->
    <script src="<?php echo base_url();?>assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
  
	  <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/echarts.min.js"></script>
	<!-- <script type="text/javascript">
	var BASE_URL = "<?php echo base_url();?>";
	</script>
  <script src="<?php echo base_url();?>assets/js/lista_anuncios.js"></script> -->
  
  <script>
  $(document).ready(function() {


    function init_stats(date_from, date_until) {
        $.ajax({
        url: "<?php echo base_url();?>api/get_ad_metrics",
        method: "GET",
        dataType: "json",
        data: {'date_from': date_from, 'date_until': date_until, 'ad': '<?php echo $anuncio["id"]; ?>'}
        }).done(function(resposta) {
          //console.log();
          init_visits_chart(resposta);
          init_sales_chart(resposta);
          init_conversionrate_chart(resposta);
        });
            
    }

    function init_conversionrate_chart(resposta) {
      var ctx = document.getElementById("conversionLineChart");
      var lineChart = new Chart(ctx, {
          type: 'line',
          options: {  
              responsive: true,
              maintainAspectRatio: false,
              scales : {
                  xAxes : [ {
                      gridLines : {
                          display : false
                      }
                  } ]
              }
          },
          data: {
              labels: resposta.dias,
              datasets: [{
              label: "% conversão",
              backgroundColor: "rgba(38, 185, 154, 0.31)",
              borderColor: "rgba(38, 185, 154, 0.7)",
              pointBorderColor: "rgba(38, 185, 154, 0.7)",
              pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointBorderWidth: 1,
              data: resposta.conversion_rate
              }]
          },
        }); 
      }

    function init_visits_chart(resposta) {
      var ctx = document.getElementById("visitsLineChart");
      var lineChart = new Chart(ctx, {
          type: 'line',
          options: {  
              responsive: true,
              maintainAspectRatio: false,
              scales : {
                  xAxes : [ {
                      gridLines : {
                          display : false
                      }
                  } ]
              }
          },
          data: {
              labels: resposta.dias,
              datasets: [{
              label: "Visitas",
              backgroundColor: "rgba(38, 185, 154, 0.31)",
              borderColor: "rgba(38, 185, 154, 0.7)",
              pointBorderColor: "rgba(38, 185, 154, 0.7)",
              pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointBorderWidth: 1,
              data: resposta.visitas
              }]
          },
        }); 
      }

    function init_sales_chart(resposta) {
      total = resposta.vendas.reduce(function(acc, val) { return acc + val; }, 0);

      total_faturamento = resposta.faturamento.reduce(function(acc, val) { return acc + val; }, 0);
      total_faturamento = total_faturamento.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});

      total_comissao = resposta.comissao.reduce(function(acc, val) { return acc + val; }, 0);
      total_comissao = total_comissao.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
      
      avg = (total / resposta.vendas.length) || 0;
      avg = avg.toLocaleString('pt-BR', {
        minimumIntegerDigits: 1,
        minimumFractionDigits: 2,      
        maximumFractionDigits: 2,
      });

      $(".count_sales").html(total);
      $(".count_renevue").html(total_faturamento);
      $(".count_fee").html(total_comissao);
      $(".count_sales_average").html(avg);

      var ctx = document.getElementById("salesLineChart");
      var lineChart = new Chart(ctx, {
          type: 'line',
          options: {  
              responsive: true,
              maintainAspectRatio: false,
              scales : {
                  xAxes : [ {
                      gridLines : {
                          display : false
                      }
                  } ]
              }
          },
          data: {
              labels: resposta.dias,
              datasets: [{
              label: "Vendas",
              backgroundColor: "rgba(38, 185, 154, 0.31)",
              borderColor: "rgba(38, 185, 154, 0.7)",
              pointBorderColor: "rgba(38, 185, 154, 0.7)",
              pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
              pointHoverBackgroundColor: "#fff",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointBorderWidth: 1,
              data: resposta.vendas
              }]
          },
      }); 
    }

    /**
     * Date range picker
     *
     * @return void
     */
    function init_daterangepicker() {

        if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
        console.log('init_daterangepicker');

        var cb = function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        };

        var optionSet1 = {
        startDate: moment().subtract(14, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2099',
        dateLimit: {
            days: 60
        },
        showDropdowns: false,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
            'Últimos 30 dias': [moment().subtract(31, 'days'), moment()],
            'Este mês': [moment().startOf('month'), moment().endOf('month')],
            'Mês passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'DD/MM/YYYY',
        separator: ' to ',
        showCustomRangeLabel: true,
        locale: {
            applyLabel: 'Aplicar',
            cancelLabel: 'Limpar',
            fromLabel: 'De',
            toLabel: 'Até',
            customRangeLabel: 'Outro',
            daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1
        }
        };

        $('#reportrange span').html(moment().subtract(14, 'days').format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
        console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
        console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('DD/MM/YYYY') + " to " + picker.endDate.format('DD/MM/YYYY'));
            startDate = picker.startDate.format('YYYY-MM-DD');
            endDate = picker.endDate.format('YYYY-MM-DD');

            init_stats(startDate, endDate);
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
        console.log("cancel event fired");
        });
        $('#options1').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
        $('#reportrange').data('daterangepicker').remove();
        });

    }
    /** END: Date range picker */

    var date_until = new Date()
    var date_from = date = new Date(new Date().setDate(new Date().getDate() - 14));
    var date_until = date_until.toISOString().slice(0, 10);
    var date_from = date_from.toISOString().slice(0, 10);

    init_stats(date_from, date_until); 
    init_daterangepicker();
    $("#home-tab").click();
  
});
/* END: document ready */

  </script>
    
  </body>
</html>