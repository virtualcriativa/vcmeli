<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Login</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>/assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.min.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
  </head>

  <body class="login">
    <div>
     

      <div class="login_wrapper">
    

        <div id="register" class="form registration_form" style="opacity: 1">
        <?php 
		if($message)
		{
		   echo '<div class="alert alert-warning" role="alert">'.$message.'</div>';
		}
		?>
          <section class="login_content">
            <?php 
            $attributes = array('class' => 'register', 'id' => 'register');
            echo form_open('/auth/create_user', $attributes); 
            ?>
            
              <h1>Crie sua Conta</h1>
			        <div>
                <input type="text" name="first_name" class="form-control" placeholder="Nome" required value="<?php echo set_value('first_name'); ?>" />
              </div>
              <div>
                <input type="text" name="last_name" class="form-control" placeholder="Sobrenome" required value="<?php echo set_value('last_name'); ?>" />
              </div>
              <div>
                <input type="email" name="email" class="form-control" placeholder="Email" required value="<?php echo set_value('email'); ?>" />
              </div>
              <div>
                <input type="text" name="phone" class="form-control" placeholder="Telefone" required value="<?php echo set_value('phone'); ?>" />
              </div>
              <div>
                <input type="text" name="company" class="form-control" placeholder="Empresa" required value="<?php echo set_value('company'); ?>" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Senha" required />
              </div>
			        <div>
                <input type="password" name="password_confirm" class="form-control" placeholder="Repita sua senha" required />
              </div>
              <div>
                <input type="submit" class="btn btn-default submit" name="Enviar" value="Enviar">
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Já possui uma conta?
                  <a href="#signin" class="to_register"> Acesse </a>
                </p>

                <div class="clearfix"></div>
                <br />

              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>