<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Login App</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>/assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.min.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
        <?php 
        if( $this->session->flashdata('authError') )
        {
          echo '<div class="alert alert-warning" role="alert">'.$this->session->flashdata('authError').'</div>';
        }
        echo $this->session->flashdata('message');
        ?>
          <section class="login_content">
            <?php 
            $attributes = array('class' => 'login', 'id' => 'login');
            echo form_open('auth/login', $attributes); 
            ?>
              <h1>Login</h1>
			        <?php echo validation_errors(); ?>
              <div>
                <input type="text" class="form-control" placeholder="Email" name="identity" required="" value="juliano@virtualcriativa.com.br<?php echo set_value('email'); ?>" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Senha" name="password" required="" value="12345678<?php echo set_value('senha'); ?>" />
              </div>
              <p>
                <?php echo "Lembrar";?>
                <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
              </p>
              <div>
                <a class="btn btn-default submit" onClick="document.getElementById('login').submit();">Log in</a>
                <a class="reset_pass" href="<?php echo base_url();?>user/password_forgot">Esqueceu sua senha?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Novo por aqui?
                  <a href="#signup" class="to_register"> Crie sua conta </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="<?php echo base_url();?>/assets/images/vcprice-logo-login.png" alt="VC Price Monitoramento de Preço"></h1>
                  <p>©2020 Todos os direitos reservados. Virtual Criativa</p>
                  <p>
                  <a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=vcprice.com.br','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/vcprice.com.br" /></a>
                  </p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
		<?php 
		if( $this->session->flashdata('authError') )
		{
		   echo $this->session->flashdata('authError');
		}
		?>
          <section class="login_content">
            <?php 
            $attributes = array('class' => 'register', 'id' => 'register');
            echo form_open('/auth/create_user', $attributes); 
            ?>
            
              <h1>Crie sua Conta</h1>
			        <div>
                <input type="text" name="first_name" class="form-control" placeholder="Nome" required value="<?php echo set_value('first_name'); ?>" />
              </div>
              <div>
                <input type="text" name="last_name" class="form-control" placeholder="Sobrenome" required value="<?php echo set_value('last_name'); ?>" />
              </div>
              <div>
                <input type="email" name="email" class="form-control" placeholder="Email" required value="<?php echo set_value('email'); ?>" />
              </div>
              <div>
                <input type="text" name="phone" class="form-control" placeholder="Telefone" required value="<?php echo set_value('phone'); ?>" />
              </div>
              <div>
                <input type="text" name="company" class="form-control" placeholder="Empresa" required value="<?php echo set_value('company'); ?>" />
              </div>
              <div>
                <input type="password" name="password" class="form-control" placeholder="Senha" required />
              </div>
			        <div>
                <input type="password" name="password_confirm" class="form-control" placeholder="Repita sua senha" required />
              </div>
              <div>
                <input type="submit" class="btn btn-default submit" name="Enviar" value="Enviar">
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">Já possui uma conta?
                  <a href="#signin" class="to_register"> Acesse </a>
                </p>

                <div class="clearfix"></div>
                <br />

              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>