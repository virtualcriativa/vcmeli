<div class="listings_filter">
    <div class="x_content">
        <h3>Filtre sua lista</h3>
        <div class="filter">

         <form action="./lista" name="filtro" id="filtro_perdendo" method="get">
                    
            <div class="form-group row">
                <div class="col-md-2 col-sm-3 ">
                    <h4>Status</h4>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="order.status[]" value="paid" > Pago
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="order.status[]" value="cancelled" > Cancelado
                        </label>
                    </div>
                    
                </div>


                <div class="col-md-2 col-sm-3 ">
                    <h4>Ordenar por</h4>
                    <select class="form-control" name="sort">
                        <option value="date_desc" <?php echo (isset($_GET['sort']) && $_GET['sort']=='date_desc')?"selected":""; ?>>Mais recentes</option>
                        <option value="date_asc"  <?php echo (isset($_GET['sort']) && $_GET['sort']=='date_asc')?"selected":""; ?>>Mais antigos</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </form>
        </div>
    </div>			                      
</div>