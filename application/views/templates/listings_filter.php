<div class="listings_filter">
    <div class="x_content">
        <h3>Filtre sua lista</h3>
        <div class="filter">

         <form action="<?php echo base_url();?>anuncios/lista" name="filtro" id="filtro_perdendo" method="get">
                    
            <div class="form-group row">
                <div class="col-md-2 col-sm-6 ">
                    <h4>Status</h4>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="status[]" value="active" <?php echo (isset($_GET["status"]) && in_array('active', $_GET["status"]))?"checked":""; ?>> Ativo
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="labels[]" value="without_stock" <?php echo (isset($_GET["status"]) && in_array('without_stock', $_GET["status"]))?"checked":""; ?>> Sem Estoque
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="status[]" value="paused" <?php echo (isset($_GET["status"]) && in_array('paused', $_GET["status"]))?"checked":""; ?>> Pausado
                        </label>
                    </div>
                </div>


                <div class="col-md-2 col-sm-6 ">
                    <h4>Tipo de Anúncio</h4>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="listing_type_id[]" value="free" <?php echo (isset($_GET["listing_type_id"]) && in_array('free', $_GET["listing_type_id"]))?"checked":""; ?>> Grátis
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="listing_type_id[]" value="gold_special" <?php echo (isset($_GET["listing_type_id"]) && in_array('gold_special', $_GET["listing_type_id"]))?"checked":""; ?>> Clássico
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" name="listing_type_id[]" value="gold_pro" <?php echo (isset($_GET["listing_type_id"]) && in_array('gold_pro', $_GET["listing_type_id"]))?"checked":""; ?>> Premium
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4 col-sm-6 ">
                    <label for="q">Pesquisar</label>
                    <input type="text" class="form-control" name ="q[]" value='<?php echo (isset($_GET["q"]))?$_GET["q"][0]:""; ?>' /> 
                </div>
            </div>

            <div class="form-group row">
                <button type="submit" class="btn btn-success">Filtrar</button>
            </div>
        </form>
        </div>
    </div>			                      
</div>