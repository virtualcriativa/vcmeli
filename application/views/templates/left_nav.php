		<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url();?>dashboard" class="site_title"><img src="<?php echo base_url();?>assets/images/app-vc-price-white.png" title="VC Price" ></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <!--<div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>-->
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php echo $_SESSION['nome_usuario'];?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                  <li><a href="<?php echo base_url();?>dashboard"><i class="fa fa-home"></i> Painel </a>
                  </li>
                  <li><a href="<?php echo base_url();?>anuncios/lista"><i class="fa fa-file"></i> Anúncios </a>
                  </li>
                  <li><a href="<?php echo base_url();?>questions/list"><i class="fa fa-file"></i> Perguntas </a>
                  </li>
                  <li><a href="<?php echo base_url();?>mercadolivre/performance"><i class="fa fa-file"></i> Performance </a>
                  </li>
                  
                  <li><a><i class="fa fa-desktop"></i> Vendas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo base_url();?>pedidos/produtos_vendidos"> Produtos Vendidos </a>
                      </li>
                      <!--<li><a href="<?php echo base_url();?>pedidos/lista"> Pedidos </a>
                      </li>-->
                    </ul>
                  </li>

                  <!--
                  <li><a href="<?php echo base_url();?>app/price_list/amazon"><i class="fa fa-desktop"></i> Amazon </a></li>
                  <li><a><i class="fa fa-desktop"></i> Mercado Livre <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="<?php echo base_url();?>mercadolivre/price_list/mercadolivre">Lista de Produtos</a>
                            </li>
                            <li><a href="<?php echo base_url();?>mercadolivre/matching">Associação de produtos</a>
                            </li>
                          </ul>  
                  </li> 
                -->
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
<!--             <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>