<?php //print_r(($anuncio)); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>VC Meli | Anúncios</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>/assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url();?>/assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Switchery -->
    <link href="<?php echo base_url();?>/assets/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
  
    <!-- iCheck -->
    <link href="<?php echo base_url();?>/assets/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">

    <link href="<?php echo base_url();?>/assets/css/anuncios.css" rel="stylesheet">


    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png">
	<script src="https://kit.fontawesome.com/3567375b2c.js" crossorigin="anonymous"></script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <?php
        include_once "templates/left_nav.php";
        ?>
        <!-- top navigation -->
        <?php
        include_once "templates/top_nav.php";
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Detalhes do Anúncio</h3>
              </div>
            </div>
            
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $anuncio["id"]; ?></h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div class="col-md-5 col-sm-5 ">
                      <div class="product-image text-center">
                        <img src="<?php echo $anuncio["pictures"][0]->secure_url; ?>" style="width: auto; max-height: 200px" />
                      </div>
                      <?php if(count($anuncio["pictures"])>1): ?>
                      <div class="product_gallery">
                        <a>
                          <img src="<?php echo $anuncio["pictures"][1]->secure_url; ?>"  />
                        </a>
                      
                      </div>
                      <?php endif; ?>
                    </div>

                    <div class="col-md-7 col-sm-7 " style="border:0px solid #e5e5e5;">

                      <h3 class="prod_title"><?php echo $anuncio["title"]; ?></h3>
                      <span class=""><?php echo constant($anuncio["shipping"]->logistic_type); ?>
                      <p><?php echo isset($anuncio["descricao"]["plain_text"])?nl2br($anuncio["descricao"]["plain_text"]):""; ?></p>
                      <br />



                    </div>



                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- GTIN -->
        <div class="right_col" role="" style="min-height: auto !important">
          <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Código Universal do Produto</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <?php 
                  $indice = array_search("GTIN", array_column($anuncio["attributes"], 'id'));
                  $gtin = (isset($anuncio["attributes"][$indice]->values[0]->name)?$anuncio["attributes"][$indice]->values[0]->name:"");

                  ?>
                  <input type="text" name="GTIN" value="<?php echo $gtin; ?>" class="form-control" />
                 
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- / GTIN -->

        <!-- FICHA TÉCNICA -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Ficha Técnica</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content ficha_tecnica">
                  <?php //print_r($atributos);die; ?>
                  <?php foreach($atributos as $atributo):?>
                    <?php 
                      $indice = array_search($atributo->id, array_column($anuncio["attributes"], 'id')); 
                      $is_hidden = isset($atributo->tags->hidden)?true:false;

                      if($is_hidden || $atributo->id=="GTIN")
                        continue;
                      
                      ($atributo->id=="COLOR")?print_r($atributo):"";

                      $css_class = isset($atributo->tags->multivalued)?"multivalue":$atributo->value_type;
                      $css_cols = $css_class=="multivalue"?"col-md-12":"col-md-6";
                      ?>
                    <div class="atributo <?php echo $css_cols; ?> <?php echo $css_class; ?>">
                      <?php 
                      if(is_int($indice) && !$is_hidden)
                      {
                        echo $model->formata_campo($atributo, $anuncio["attributes"][$indice]); 
                      }
                      elseif(!$is_hidden)
                      {
                        echo $model->formata_campo($atributo, "");
                      }
                      
                      ?>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- / FICHA TÉCNICA -->

        <!-- STATS -->
        <div class="right_col" role="" style="min-height: auto !important">
          <div class="row">
            <div class="col-md-12 col-sm-12  ">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Vendas<small>no período</small></h2>
                  
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div id="resposta">r</div>
                <ul class="nav nav-tabs bar_tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Vendas</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <div style="height: 250px">  
                    <canvas id="salesLineChart"></canvas>
                  </div>
                  </div>
                  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                        booth letterpress, commodo enim craft beer mlkshk aliquip
                  </div>
                  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    xxFood truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid. Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan four loko farm-to-table craft beer twee. Qui photo
                        booth letterpress, commodo enim craft beer mlkshk 
                  </div>
                </div>
                  
                </div>
              </div>
            </div>
        </div>
        <!-- / STATS -->

        <!-- footer content -->
        <?php
        include_once "templates/footer.php";
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url();?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url();?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?php echo base_url();?>assets/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- jQuery Sparklines -->
    <script src="<?php echo base_url();?>assets/vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- Flot -->
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?php echo base_url();?>assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url();?>assets/vendors/DateJS/build/date.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url();?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Switchery -->
    <script src="<?php echo base_url();?>assets/vendors/switchery/dist/switchery.min.js"></script>
  
    <!-- jQuery Tags Input -->
    <script src="<?php echo base_url();?>assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
  
    <!-- iCheck -->
    <script src="<?php echo base_url();?>assets/vendors/iCheck/icheck.min.js"></script>
  
	  <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url();?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
	
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script src="<?php echo base_url();?>assets/js/echarts.min.js"></script>
	<!-- <script type="text/javascript">
	var BASE_URL = "<?php echo base_url();?>";
	</script>
  <script src="<?php echo base_url();?>assets/js/lista_anuncios.js"></script> -->
  
  <script>
  $(document).ready(function() {

    $.ajax({
      url: "<?php echo base_url();?>api/get_ad_metrics",
      method: "GET",
      dataType: "json",
      data: {'date_from': '2021-05-01', 'date_until': '2021-05-31', 'ad': '<?php echo $anuncio["id"]; ?>'}
    }).done(function(resposta) {
      //$( "#resposta" ).html( data );
      console.log(resposta.dias);

      var ctx = document.getElementById("salesLineChart");
      var lineChart = new Chart(ctx, {
      type: 'line',
      options: {  
          responsive: true,
          maintainAspectRatio: false,
          scales : {
              xAxes : [ {
                  gridLines : {
                      display : false
                  }
              } ]
          }
      },
      data: {
        labels: resposta.dias,
        datasets: [{
        label: "Vendas",
        backgroundColor: "rgba(38, 185, 154, 0.31)",
        borderColor: "rgba(38, 185, 154, 0.7)",
        pointBorderColor: "rgba(38, 185, 154, 0.7)",
        pointBackgroundColor: "rgba(38, 185, 154, 0.7)",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointBorderWidth: 1,
        data: resposta.vendas
        }]
      },
    });




    });

    
});

  </script>
    
  </body>
</html>