<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

/* 	private $redirect_uri = 'https://localhost/mercadoapp/user/authorize_meli';
	private $auth_url;
	private $meli_access_token; */

	public function __construct()
	{

		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->library('mongo_db');
		
	}


	public function is_trial_expired()
	{
		if($this->session->status_cliente==TRIAL_EXPIRED)
		{
			$this->session->message_type = "warning";
			$this->session->message = "Seu período de testes já terminou. Você precisa assinar um plano para continuar utilizando o sistema.";
			redirect('/user/account');
		}	
	}

	public function is_logged()
	{
		if(!is_string($this->session->user_id) && !is_string($this->session->nome_usuario))
		{
			session_destroy();
			redirect('/');
		}
	}


	/**
	 * Método que atualiza os dados do usuário na tabela users_info
	 *
	 * @param [int] $user_id
	 * @param array $info
	 * @return void
	 */
	public function update_user_info($user_id, $info = array())
	{
		$this->mongo_db
        ->where(array('user'=>$user_id))
        ->set($info)
        ->update('users_info', array('upsert' => true));
	}

	public function get_user_data()
	{
		$info = $this->mongo_db
		->where(array('user'=>$this->session->user_id))
		->limit(1)
		->get('users_info');
		
		return (isset($info[0]))?$info[0]:$info;
	}



}
