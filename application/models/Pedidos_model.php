<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_model extends CI_Model {

	private $limit_db_results = 10;
	private $filtro = array();
	private $end_month_date;
	private $start_month_date;
	private $meli_user_id;
	private $refresh_token_attemps = 0;

	private $start_date;
	private $end_date;

	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
			$this->meli_user_id = $this->session->userdata["meli_user"]["meli_user_id"];
	}

	public function set_start_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function set_end_date($end_date)
	{
		$this->end_date = $end_date;
	}

	
	public function set_filtro($filtros = array())
	{

		foreach($filtros as $filtro => $valor)
		{
			$filtro = str_replace("_", ".", $filtro);
			if(is_array($valor))
				$this->filtro[] = $filtro . '=' . implode('|', $valor);
			else	
				$this->filtro[] = $filtro . '=' . $valor;
		}
		$this->filtro = implode('&', $this->filtro);

		return $this;

	}
	
	public function get_lucro_mensal($mes, $ano)
    {
		$this->get_intervalo_mensal($mes, $ano);

        $documents = $this->mongo_db
        ->where_between('venda.date_created', $this->start_month_date ,$this->end_month_date)
		->where('venda.status', 'paid')
		->where('meli_client_id', $this->meli_user_id)
		->get("vendas");

		$c_total = 0;
		foreach($documents as $key => $document)
		{	
			//print_r(new MongoDB\BSON\ObjectId($document["_id"]->{'$id'}));
			//die;
			$_id = $document["_id"]->{'$id'};
			$custos = $this->mongo_db
			->where('_id', new MongoDB\BSON\ObjectId($_id))
			->get("produtos_custos");
			$documents[$key]["custos"] = $custos;
			$c_total += isset($document["venda"]->order_items[0]->sale_fee)?$document["venda"]->order_items[0]->sale_fee:0;
		}

        return $documents;
	}

	public function get_comissoes_mensais($mes, $ano)
    {
		$this->get_intervalo_mensal($mes, $ano);
		
        $documents = $this->mongo_db
        //->select(array())
        ->where_between('venda.date_created', $this->start_month_date ,$this->end_month_date)
		->where('venda.status', 'paid')
		->where('meli_client_id', $this->meli_user_id)
		->get("vendas");

		$c_total = 0;
		foreach($documents as $document)
		{
			$c_total += isset($document["venda"]->order_items[0]->sale_fee)?$document["venda"]->order_items[0]->sale_fee:0;
		}

        return $c_total;
	}

	public function get_total_sales_number($mes = null, $ano = null)
    {
		//$this->get_intervalo_mensal($mes, $ano);
		$this->format_date_interval();
		
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');
			
		$url = MELI_API_ROOT_URL.'/orders/search?seller='.$this->session->meli_user["meli_user_id"] .'&order.date_created.from='.$this->start_date.'&order.date_created.to='.$this->end_date.'&order.status=paid';

		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$anuncios = $this->meli->get($url);

		if(isset($anuncios['body']->error) || !isset($anuncios['body']))
        {
			$this->refresh_token_attemps ++;
			$this->meli_model->refresh_token();
			return $this->get_total_anuncios();
		}

		return (isset($anuncios["body"]->paging->total))?$anuncios["body"]->paging->total:null;
	}

	public function get_sales_from_range($mes = null, $ano = null)
    {
		//$this->get_intervalo_mensal($mes, $ano);
		$this->format_date_interval();
		
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');
			
		$url = MELI_API_ROOT_URL.'/orders/search?seller='.$this->session->meli_user["meli_user_id"] .'&order.status=paid&filters=WITH_DATE_CLOSED_1M_OLD';

		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$anuncios = $this->meli->get($url);
		print_r($url);
		$teste =  (array)$anuncios["body"]->results;
		print_r(array_column($teste, "order_items"));
		print_r($teste);
		die;
		if(isset($anuncios['body']->error) || !isset($anuncios['body']))
        {
			$this->refresh_token_attemps ++;
			$this->meli_model->refresh_token();
			return $this->get_total_anuncios();
		}

		return (isset($anuncios["body"]))?$anuncios["body"]:null;
	}

	public function get_vendas_mensais($mes, $ano)
    {
		$this->get_intervalo_mensal($mes, $ano);
		
        $documents = $this->mongo_db
        //->select(array())
        ->where_between('venda.date_created', $this->start_month_date ,$this->end_month_date)
		->where('venda.status', 'paid')
		->where('meli_client_id', $this->meli_user_id)
		->get("vendas");

		$c_total = 0;

		foreach($documents as $document)
		{
			$c_total += isset($document["venda"]->order_items[0]->quantity)?$document["venda"]->order_items[0]->quantity*$document["venda"]->order_items[0]->unit_price:0;
		}

        return $c_total;
	}
	
	public function get_vendas_produtos_custos($mes, $ano)
    {
		$this->get_intervalo_mensal($mes, $ano);

        $documents = $this->mongo_db
        //->select(array("custos"))
		->where_between('venda.date_created', $this->start_month_date ,$this->end_month_date)
        ->where(array('venda.status' => 'paid', 'meli_client_id' => $this->session->meli_user["meli_user_id"]))
		->get("vendas_custos");

		$c_total = 0;
		//$c=1;

		$lucro_total = 0;
		foreach($documents as $document)
		{
			//$c = $c + 1;
			//$venda = $document["venda"]->order_items[0]->quantity*$document["venda"]->order_items[0]->unit_price;
			//$custo = $document["custos"][0]->custo_total;
			//$lucro = $venda - $custo;
			//$lucro_total = $lucro_total + $lucro;
			//print_r("venda: ".$venda." Custo: ".$custo." Lucro: ".$lucro);
			//print("<br>");
			$c_total += isset($document["custos"][0]->custo_total)?$document["custos"][0]->custo_total:0;
		}
		//print($lucro_total);
		//print_r($c);
		//die;
		//209
        return $c_total;
	}
	
	private function get_intervalo_mensal($mes, $ano)
	{
		$dia = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		$this->end_month_date = $ano."-".$mes."-".$dia."T23:59:59.000-00:00";
		$this->start_month_date = $ano."-".$mes."-01T00:00:00.000-00:00"; 
		return $this;
	}


	/**
	 * Método que retorna a quantidade total de anúncios de um usuário
	 * 
	 *
	 * @return int
	 */
	public function get_total_pedidos()
	{
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		$url = MELI_API_ROOT_URL.'/orders/search?seller='.$this->session->meli_user["meli_user_id"];
		
		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$pedidos = $this->meli->get($url);

		/*
		$pedidos = $this->mongo_db
		->where('pedido.seller_id', (int) $this->session->active_seller_id)
		->get('pedidos'); 
        */
		return $pedidos["body"]->paging->total;
	}


    public function get_pedidos_from_meli($limit = '20', $offset = '0')
    {
		$url = MELI_API_ROOT_URL.'/orders/search?seller='.$this->session->meli_user["meli_user_id"].'&limit='.$limit.'&offset='.$offset;
		
		if(is_string($this->filtro))
            $url .= '&' . $this->filtro;
        else
			$url .= '&' .  '&sort=date_desc';
			
		//print_r($_GET);
		//print_r(array_column($_GET['order_status'], 'order_status'));
		//var_dump(array_search("paid", $_GET["order_status"]));
		//var_dump(array_search("cancelled", $_GET["order_status"]));
		//die;

		$response = $this->meli->get($url);
	
        // @todo verificar se houve uma resposta válida. Em caso negativo, 
        // checar a troca do token com o método abaixo 
        // $this->check_config();


        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();

            redirect('pedidos/lista', 'refresh');
        }
            
        
		$pedidos = array();
		
		if(!count($response["body"]->results))
			return $pedidos;

        $pedidos = $response["body"]->results;

        return $pedidos;
	}

	/**
	 * Metodo que recebe uma notificação como parâmetro
	 * para que seja verificada a alteração
	 * Os dados da notificação são utilizados na chamada à API do Mercado Livre
	 * para pegar os dados atualizados da venda
	 *
	 * @param [type] $notification
	 * @return void
	 */
	public function notify_change($notification)
	{
		//print_r($notification);
		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');
		
		$url = MELI_API_ROOT_URL.$notification["resource"];
		$params = $this->meli_model->login_meli($notification["user_id"]);
		
		$response = $this->meli->get($url);

		if(isset($response['body']->error))
        {
			try
			{
				$this->refresh_token_attemps++;
				$this->meli_model->refresh_token();
				return $this->notify_change($notification);
			}
			catch (Exception $e) 
			{
				$this->load->view('errors/token_error', array("message" => $e->getMessage()));
				return;
			}
			
		}

		$this->update_order_changes($response);
	}

	/**
	 * Método que atualiza a venda a partir da notificação de alteração
	 *
	 * @param [type] $venda
	 * @return void
	 */
	private function update_order_changes($venda)
	{
		$insert = array(
			"venda" => $venda["body"],
			"client_app_id" => $this->session->meli_user["user_id"],
			"meli_client_id" => $venda["body"]->seller->id,
			"id_venda" => $venda["body"]->id
		);

		// Verifica se a venda já existe, caso não exista,
		// precisamos inserir a venda e adicionar o custo atual do bling
		$select = $this->mongo_db
		->where('venda.id', $venda["body"]->id)
		->get('vendas');

		if(!count($select))
		{
			$result = $this->mongo_db
			->where('venda.id', $venda["body"]->id)
			->set((Array)$insert)
			->update('vendas', array('upsert' => true));

			$select = $this->mongo_db
			->where('venda.id', $venda["body"]->id)
			->get('vendas');

			$this->load->model('bling_model');
			$this->load->model('anuncios_model');
			foreach($venda["body"]->order_items as $key => $item)
			{
				$anuncio = $this->anuncios_model->get_anuncio_detalhes(array($item->item->id));
				$anuncio = (array)$anuncio[0]->body;
				$custos = $this->bling_model->get_custo($anuncio);
				$custos = $this->bling_model->save_custo($custos, $select);
			}

		}
		else
		{
			$result = $this->mongo_db
			->where('venda.id', $venda["body"]->id)
			->set((Array)$insert)
			->update('vendas', array('upsert' => true));
		}


        $this->api_model->update_notification_result($result);
	}

	private function format_date_interval()
	{
		$this->end_date = $this->end_date->format('Y')."-".$this->end_date->format('m')."-".$this->end_date->format('d')."T23:59:59.000-00:00";
		$this->start_date = $this->start_date->format('Y')."-".$this->start_date->format('m')."-".$this->start_date->format('d')."T00:00:00.000-00:00"; 
		return $this;
	}


}
