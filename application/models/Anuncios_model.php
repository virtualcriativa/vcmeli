<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anuncios_model extends CI_Model {

	private $limit_db_results = 10;
	private $filtro = array();
	private $refresh_token_attemps = 0;

	private $start_month_date;
	private $end_month_date;

	private $start_year_date;
	private $end_year_date;

	private $start_day_date;
	private $end_day_date;

	private $start_date;
	private $end_date;


	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
	}

	public function set_start_month_date($start_month_date)
	{
		$this->start_month_date = $start_month_date;
	}

	public function set_end_month_date($end_month_date)
	{
		$this->end_month_date = $end_month_date;
	}

	public function set_start_year_date($start_year_date)
	{
		$this->start_year_date = $start_year_date;
	}

	public function set_end_year_date($end_year_date)
	{
		$this->end_year_date = $end_year_date;
	}

	public function set_start_day_date($start_day_date)
	{
		$this->start_day_date = $start_day_date;
	}

	public function set_end_day_date($end_day_date)
	{
		$this->end_day_date = $end_day_date;
	}

	public function set_start_date($start_date)
	{
		$this->start_date = $start_date;
	}

	public function set_end_date($end_date)
	{
		$this->end_date = $end_date;
	}

	
	public function set_filtro($filtros = array())
	{	
		foreach($filtros as $filtro => $valor)
		{
			$this->filtro[] = $filtro . '=' . urlencode(implode(',', $valor));
		}
		
		$this->filtro = implode('&', $this->filtro);

		return $this;

	}

	public function get_total_visits()
	{
		//$this->get_intervalo_mensal($mes, $ano);
		$this->format_date_interval();

		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');
			
		$url = MELI_API_ROOT_URL.'/users/'.$this->session->meli_user["meli_user_id"] .'/items_visits?date_from='.$this->start_date.'&date_to='.$this->end_date;

		if(is_string($this->filtro))
			$url .= $this->filtro . '&';

		$anuncios = $this->meli->get($url);

		if(isset($anuncios['body']->error) || !isset($anuncios['body']))
        {
			//print_r($anuncios['body']);
			$this->refresh_token_attemps ++;
			try 
            {
                $this->meli_model->refresh_token();
            }
            catch (Exception $e)
            {
                $this->meli_model->send_to_auth();
            }
			
			return $this->get_total_anuncios();
		}

		return (isset($anuncios["body"]->total_visits))?$anuncios["body"]->total_visits:null;
	}


	/**
	 * Método que retorna a quantidade total de anúncios de um usuário
	 * 
	 *
	 * @return int
	 */
	public function get_total_anuncios()
	{
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');

		$url = MELI_API_ROOT_URL.'/users/'.$this->session->meli_user["meli_user_id"] .'/items/search?';
		
		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$anuncios = $this->meli->get($url);
		
		if(isset($anuncios['body']->error) || !isset($anuncios['body']))
        {
			
			$this->refresh_token_attemps ++;
			$this->meli_model->refresh_token();
			return $this->get_total_anuncios();
		}

		return (isset($anuncios["body"]->paging->total))?$anuncios["body"]->paging->total:null;
	}


	public function get_stats()
	{
		$url = MELI_API_ROOT_URL.'/sites/MLB/search?seller_id='.$this->session->meli_user["meli_user_id"].'&limit=1&sort=biggest_sold_quantity';
		$response = $this->meli->get($url);

        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
            return $this->get_stats();
        }
        
		return $response['body']->seller;
		
	}

	public function get_mais_vendidos($limit = 5)
	{
		$url = MELI_API_ROOT_URL.'/sites/MLB/search?seller_id='.$this->session->meli_user["meli_user_id"].'&limit='.$limit.'&sort=biggest_sold_quantity';
		$response = $this->meli->get($url);

        // @todo verificar se houve uma resposta válida. Em caso negativo, 
        // checar a troca do token com o método abaixo 
		// $this->check_config();
		
        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
            return $this->get_mais_vendidos($limit);
        }
        
		return $response['body']->results;
		
	}

	public function get_mais_vendidos_db($limit = 5)
	{
		$mais_vendidos = $this->mongo_db
			//->select(array('_id', $this->identity_column))
		    ->where('anuncio.seller_id', $this->session->meli_user["meli_user_id"])
			->order_by(array('anuncio.sold_quantity' => 'ASC'))
			->limit($limit)
			->get('anuncios');
			
		return $mais_vendidos;
	}

    public function get_anuncios_from_meli($limit = '30', $offset = '0')
    {
		$url = MELI_API_ROOT_URL.'/users/'.$this->session->meli_user["meli_user_id"] .'/items/search?include_filters=true&limit='.$limit.'&offset='.$offset;
		//$url = MELI_API_ROOT_URL.'/users/'.$this->session->meli_user["meli_user_id"] .'/items/search?include_filters=true';
		

		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		//print_r($url);
		//die;
		$response = $this->meli->get($url);


        // @todo verificar se houve uma resposta válida. Em caso negativo, 
        // checar a troca do token com o método abaixo 
        // $this->check_config();

		
        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
            redirect('anuncios/lista', 'refresh');
        }
        
        $anuncios = array();

		if(!count($response["body"]->results))
			return $anuncios;

		$anuncios = $this->get_anuncio_detalhes($response["body"]->results);

        foreach($anuncios as $key => $anuncio)
        {
			$visitas = $this->get_total_visitas($anuncio->body->id);
            $anuncio->body->visitas = (array)$visitas;
		}

        return $anuncios;
	}

	public function get_anuncio_imagens($mlid)
    {
        $url = MELI_API_ROOT_URL.'/items?access_token='.$this->session->meli_user["access_token"].'&ids='.$mlid;
		
		$response = $this->meli->get($url);

        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
			return $this->get_anuncio_imagens($mlid);
        }
        
        $anuncios = array();

		if(!count($response["body"]))
			return $anuncios;

		$anuncios = $response["body"][0]->body->secure_thumbnail;

        return $anuncios;
	}

	public function get_anuncio_detalhes($mlids = array())
    {
		//$response = $this->meli->get(MELI_API_ROOT_URL.'/items?ids='.implode(",",$mlids));
		//print_r($response);
		foreach($mlids as $mlid)
		{
			$r = $this->meli->get(MELI_API_ROOT_URL.'/items?ids='.$mlid);

			if(isset($r['body']->error) || !isset($r['body']))
			{
				$this->meli_model->refresh_token();
				return $this->get_anuncio_detalhes(array($mlids));
			}
			$response['body'][] = $r['body'][0];
		}

        return $response['body'];
	}

	public function get_anuncio_descricao($mlid)
    {
        $response = $this->meli->get(MELI_API_ROOT_URL.'/items/'.$mlid.'/descriptions');

		if($response["httpCode"]==410)
		{
			return null;
		}

		if(isset($response['body']->error))
        {
			$this->meli_model->refresh_token();
			return $this->get_anuncio_descricao($mlid);
		}

        return (isset($response["body"][0]))?(array)$response["body"][0]:null;
	}

	public function get_categoria_atributos($catid)
	{
		$response = $this->meli->get(MELI_API_ROOT_URL.'/categories/'.$catid.'/attributes');

		if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
			return $this->get_categoria_atributos($catid);
		}

        return (isset($response["body"]))?(array)$response["body"]:null;
	}

	public function category_details($catid)
	{
		$response = $this->meli->get(MELI_API_ROOT_URL.'/categories/'.$catid);

		if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();
			return $this->category_details($catid);
		}

        return (isset($response["body"]))?(array)$response["body"]:null;
	}
	
	public function get_total_visitas($mlid)
    {
        if(is_array($mlid))
            $mlid = implode(",",$mlid);

        $response = $this->meli->get(MELI_API_ROOT_URL.'/visits/items?ids='.$mlid);

        return $response["body"];
	}

	public function get_intervalo_mensal($mes, $ano, $dia = "01")
	{
		$dia = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		$this->end_month_date = $ano."-".$mes."-".$dia."T23:59:59.000-00:00";
		$this->start_month_date = $ano."-".$mes."-".$dia."T00:00:00.000-00:00"; 
		return $this;
	}

	private function format_date_interval()
	{
		$this->end_date = $this->end_date->format('Y')."-".$this->end_date->format('m')."-".$this->end_date->format('d')."T23:59:59.000-00:00";
		$this->start_date = $this->start_date->format('Y')."-".$this->start_date->format('m')."-".$this->start_date->format('d')."T00:00:00.000-00:00"; 
		return $this;
	}

	public function listing_position($mlid)
	{
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');

		$url = MELI_API_ROOT_URL.'/highlights/MLB/item/'.$mlid;
		
		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$anuncios = $this->meli->get($url);

		if((isset($anuncios['body']->error) && $anuncios['body']->error!="NOT_FOUND") || !isset($anuncios['body']))
        {
			$this->refresh_token_attemps ++;
			$this->meli_model->refresh_token();
			return $this->listing_position($mlid);
		}

		return (isset($anuncios["body"]->position))?$anuncios["body"]->position:"Não encontrado";
	}

	public function best_sellers($category_id)
	{
		if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

		if($this->refresh_token_attemps==3)
			throw new Exception('Sem comunicação para atualizar o token de comunicação com o Mercado Livre');

		$url = MELI_API_ROOT_URL.'/highlights/MLB/category/'.$category_id;
		
		if(is_string($this->filtro))
			$url .= '&' . $this->filtro;

		$anuncios = $this->meli->get($url);

		if($anuncios['httpCode'] == "404")
			return null;

		if(isset($anuncios['body']->error) || !isset($anuncios['body']))
        {
			
			$this->refresh_token_attemps ++;
			$this->meli_model->refresh_token();
			return $this->best_sellers($category_id);
		}

		return (isset($anuncios["body"]->content))?$anuncios["body"]->content:null;
	}
	
	public function formata_campo($field_data, $field_values)
	{
		$value_name = isset($field_values->value_name)?$field_values->value_name:"";
		$value_id 	= isset($field_values->value_id)?$field_values->value_id:"";

		if($field_data->value_type == "string" && !isset($field_data->tags->multivalued))
		{
			$label = '<label for=attributes['.$field_data->id.'] >'.$field_data->name.'</label>';
			$field = '<input name="attributes['.$field_data->id.']" value="'.$value_name.'" type="text" maxlength='.$field_data->value_max_length.' class=form-control />';
			return $label.$field;
		}

		if($field_data->value_type == "string" && $field_data->tags->multivalued)
		{
			$label = '<label for="attributes['.$field_data->id.']" >'.$field_data->name.'</label>';
			$field = '<input id="tags_1" name="attributes['.$field_data->id.']" value="'.$value_name.'" type="text" maxlength='.$field_data->value_max_length.' class="tags form-control" />';
			return $label.$field;
		}

		if($field_data->value_type == "number")
		{
			
			$label = '<label for="attributes['.$field_data->id.']" >'.$field_data->name.'</label>';
			$field = '<input name="attributes['.$field_data->id.']" value="'.$value_name.'" type=text maxlength='.$field_data->value_max_length.' class=form-control />';
			return $label.$field;
		}

		if($field_data->value_type == "boolean")
		{
			$checked = ($field_values->values[0]->name=="Sim")?"checked":"";
			$label = '<label for="attributes['.$field_data->id.']" >'.$field_data->name;
			$field = ' <input type="checkbox" class="js-switch" name="attributes['.$field_data->id.']" value="1" '.$checked.' /> </label>';
			return $label.$field;
		}

		if($field_data->value_type == "number_unit")
		{
			$unidade = isset($field_values->values[0]->struct->unit)?$field_values->values[0]->struct->unit:"Unidade";
			$valor = isset($field_values->values[0]->struct->number)?$field_values->values[0]->struct->number:"";
			$label = '<div class="dropdown input-group"><div class="col-md-12"><label for=attributes['.$field_data->id.'] >'.$field_data->name.'</label></div>';
			$field = '<input name="attributes['.$field_data->id.']" value="'.$valor.'" type="text" maxlength="'.$field_data->value_max_length.'" class="form-control" />';
			
			$field .= '<div style="vertical-align:bottom" class="input-group-btn">';
			$field .= '<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'.$unidade.' <span class="caret"></span>';
			$field .= '</button>';
			$field .= '<ul class="dropdown-menu dropdown-menu-right" role="menu">';
			
			foreach($field_data->allowed_units as $key => $unit_type)
			{
				$field .= '<li><a class="dropdown-item" href="#">'.$unit_type->name.'</a></li>';
			}				

			$field .= '</ul>';
			$field .= '</div>';
			$field .= '</div>';
			return $label.$field;
		}

		//<input id="tags_1" type="text" class="tags form-control" value="social, adverts, sales" />
		//<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
	}

}
