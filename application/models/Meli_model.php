<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meli_model extends CI_Model {

    /**
     * Nome da conta no Mercado Livre
     *
     * @var string
     */
    private $account_name = "";
	
	public function __construct()
	{
        parent::__construct();
        $this->load->library('mongo_db');

        $params = $this->get_meli_params();
        $this->load->library('meli', $params);
    }

    
    public function login_meli($account = NULL) 
    {
        if($account)
            $meli_data  = $this->get_user_meli_data($account);
        else
            $meli_data  = $this->get_all_user_meli_data($this->session->user_id);

        //Se existe uma conta do ml no banco de daodos
        if(count($meli_data))
        {
            $this->set_meli_session($meli_data);
        }
        else{
            $this->send_to_auth();
        }

        if($meli_data[0]["access_token"] == null)
            $this->refresh_token();

        return $this;

    }



    /**
     * Método que retona todas as informações referentes ao usuário do Mercado Livre
     * As informação estão gravadas no banco de dados e podem conter nenhuma ou várias
     * contas para um único usuário. 
     *
     * @param [string] $user_id
     * @return array
     */
    private function get_all_user_meli_data($user_id)
    {
        $document = $this->mongo_db
        ->select(array())
        //->where('_id', new MongoId($id))
        ->where('user_id', $this->session->user_id)
        ->get("meli_users");

        return $document;
    }

    /**
     * Método que retorna as informações de uma única conta do usuário no banco de dados 
     *
     * @param int $_id - o ID do usuário no Mercado Livre
     * @return array
     */
    private function get_user_meli_data($id)
    {
        $document = $this->mongo_db
        ->select(array())
        //->where('_id', new MongoId($id))
        ->where('meli_user_id', $id)
        ->limit(1)
        ->get("meli_users");

        return $document;
    }

    /**
     * Undocumented function
     *
     * @param array $meli_data
     * @return void
     */
    private function set_meli_session($meli_data = array())
    {
        $this->session->meli_user = $meli_data[0];
    }

    /**
     * Método para capturar o nome da conta para identificação no sistema
     *
     * @return void
     */
    public function get_user_account_name()
    {
        $params = $this->get_meli_params();
        $account = $this->meli->get('users/me', $params);
 
        if(isset($account["body"]->message) && $account["body"]->message == "invalid_token")
        {
            $this->refresh_token()->get_user_account_name();
        }
        
        //$this->session->account_name = $account["body"]->nickname;
        if(isset($account["body"]->nickname))
            return $account["body"]->nickname;
        else
            return null;
    }


    public function refresh_token()
    {
        //print_r($this->session);
        $meli_user_data = array($this->session->meli_user);
        $params = $this->get_meli_params();
        //print_r($params);
        //$this->load->library('meli', $params);
        $ret = $this->meli->set_refresh_token($meli_user_data[0]["refresh_token"]);
        $token = $this->meli->refreshAccessToken();
        
        if(isset($token["body"]->error))
        {
            throw new Exception($token["body"]->message.' - erro: '.$token["body"]->error);
		
        }

        $meli_user_data[0]["access_token"] = $token["body"]->access_token;
        $meli_user_data[0]["refresh_token"] = $token["body"]->refresh_token;
        
        $this->set_meli_session($meli_user_data);
        $this->update_access_token($token["body"]->access_token);

        return $this;
    }

    private function update_access_token($access_token)
    {
        $data['access_token'] = $access_token;

		$this->mongo_db
        ->where(array('meli_user_id'=>(int)$this->session->meli_user["meli_user_id"]))
        ->set($data)
		->update('meli_users', array('upsert' => true));
    }



    public function send_to_auth()
    {
        $params = $this->get_meli_params();
        $this->load->library('meli', $params);
        redirect($this->meli->getAuthUrl(REDIRECT_URI, 'https://auth.mercadolivre.com.br'), 'location');
    }

    public function authorize_meli($code)
	{
        $user = $this->meli->authorize($code,REDIRECT_URI);

        $user["body"]->_id = $this->session->user_id;

        $ml = array((array)$user["body"]);
        $this->set_meli_session($ml);
        $account_name = $this->get_user_account_name();
        $user["body"]->account_name = $account_name;
 
        $this->update_meli_user($user);
        $this->login_meli($user['body']->user_id);

		return $user;
    }
    
    public function get_meli_account_status($meli_user_id)
    {
        $document = $this->mongo_db
        ->select(array())
        //->where('_id', new MongoId($id))
        ->where('meli_user_id', $meli_user_id)
        ->get("meli_users");

        return isset($document[0]["setup"])?$document[0]["setup"]:"PENDING";
    }

    public function update_meli_user($user = array())
    {

        $data = array(
            '_id'               => $user['body']->user_id,
            'meli_user_id'      => $user['body']->user_id,
            'user_id'           => $this->session->user_id,
            'access_token'      => $user['body']->access_token,
            'expires_in'        => $user['body']->expires_in,
            'refresh_token'     => $user['body']->refresh_token,
            'account_name'      => $user["body"]->account_name,
            'setup'             => $this->get_meli_account_status($user['body']->user_id),
        );
        
		$this->mongo_db
        ->where(array('meli_user_id'=>(int)$data["meli_user_id"]))
        //->where(array('user_id'=>$data["user_id"]))
        ->set($data)
		->update('meli_users', array('upsert' => true));
    }

    public function get_meli_params()
    {
        $params['client_id']        = MELI_APP_ID;
        $params['client_secret']    = MELI_SECRET_KEY;
        $params['access_token']     = (isset($this->session->meli_user["access_token"]))?$this->session->meli_user["access_token"]:"";
        $params['refresh_token']    = (isset($this->session->meli_user["refresh_token"]))?$this->session->meli_user["refresh_token"]:"";

        return $params;
    }

    public function load_performance()
    {
        $params['client_id']        = MELI_APP_ID;
        $params['client_secret']    = MELI_SECRET_KEY;
        $params['access_token']     = (isset($this->session->meli_user["access_token"]))?$this->session->meli_user["access_token"]:"";
        $params['refresh_token']    = (isset($this->session->meli_user["refresh_token"]))?$this->session->meli_user["refresh_token"]:"";

        return $params;
    }



}
