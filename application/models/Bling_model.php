<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bling_model extends CI_Model {

	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');
	}


	public function register()
	{
		//$this->mongo_db
		//->update('integracao_bling', array('_id' => $this->session->meli_user["meli_user_id"], 'apikey' => $_POST['apikey']), array('$upsert'=>true));

		$r = $this->mongo_db
		->insert('integracao_bling', array('_id' => $this->session->meli_user["meli_user_id"], 'apikey' => $_POST['apikey']));

		$r = $this->mongo_db
		->where(array('_id'=>$this->session->meli_user["meli_user_id"]))
		->set(array('_id' => $this->session->meli_user["meli_user_id"], 'apikey' => $_POST['apikey']))
		->update('integracao_bling', array('$upsert'=>true));

		$this->session->set_flashdata('message', 'Configuração atualizada');

		redirect('dashboard', 'refresh');

	}

	public function get_apikey_config()
	{
		$document = $this->mongo_db
        ->where(array('_id'=>$this->session->meli_user["meli_user_id"]))
		->get("integracao_bling");

		return $document;
	}

	public function get_custo($anuncio = array())
	{
		//print_r($anuncio["attributes"]);
		$key = array_search("SELLER_SKU", array_column($anuncio["attributes"], 'id'));
		$sku = ($anuncio["seller_custom_field"]==NULL)?$anuncio["attributes"][$key]->value_name:$anuncio["seller_custom_field"];
		return $this->executeGetProducts($sku);
	}

	public function save_custo($produto_bling, $venda)
	{
		$produto_bling = json_decode($produto_bling);
		$_id = $venda[0]["_id"]->{'$id'};
		//$objDateTime = new DateTime('NOW');
		$objDateTime = new MongoDB\BSON\UTCDateTime();

		$data['_id'] = new MongoDB\BSON\ObjectId($_id);
		$data['bling_sku'] = $produto_bling->retorno->produtos[0]->produto->codigo;
		$data['custo_unitario'] = $produto_bling->retorno->produtos[0]->produto->precoCusto;
		$data['data_in'] = $objDateTime;

		print_r("Imprimindo dados da venda para verificar os custos");
		print_r($venda);
		foreach($venda[0]["venda"]->order_items as $key => $item)
		{
			$data['qtd'] = (int)$item->quantity;
			$data['custo_total'] = (float)$data['custo_unitario'] * $data['qtd'];
			
			//Verifica se o custo da venda já existe
			$existe = $this->mongo_db
        	->where('_id', $data['_id'])
			->get('produtos_custos');
			print_r("Resultado para verificar se o custo existe");
			var_dump($existe);

			$result = $this->mongo_db
			->insert('produtos_custos', $data);
			print_r("Custo inserido com os dados abaixo");
			var_dump($result);
			//print_r($data);
		}

	}


	function executeGetProducts($seller_sku)
	{
		$url = BLING_URL . $seller_sku . '/json';
		
		$apikey = $this->get_apikey_config();
		
		if(!$apikey)
			return null;

		$apikey = $apikey[0]["apikey"];              
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, $url . '&apikey=' . $apikey.'&estoque=S');
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec($curl_handle);
		curl_close($curl_handle);
		return $response;
	  }


}
