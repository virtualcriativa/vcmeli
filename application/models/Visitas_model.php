<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitas_model extends CI_Model {

	private $limit_db_results = 10;
	private $filtro = array();
	private $end_month_date;
	private $start_month_date;
	private $meli_user_id;


	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
			$this->meli_user_id = $this->session->userdata["meli_user"]["meli_user_id"];
	}
	
	public function set_filtro($filtros = array())
	{
		foreach($filtros as $filtro => $valor)
		{
			$filtro = str_replace("_", ".", $filtro);
			if(is_array($valor))
				$this->filtro[] = $filtro . '=' . implode('|', $valor);
			else	
				$this->filtro[] = $filtro . '=' . $valor;
		}
		$this->filtro = implode('&', $this->filtro);

		return $this;

	}
	
	private function get_intervalo_mensal($mes, $ano)
	{
		$dia = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
		$this->end_month_date = $ano."-".$mes."-".$dia."T23:59:59.000-00:00";
		$this->start_month_date = $ano."-".$mes."-01T00:00:00.000-00:00"; 
		return $this;
	}


    public function get_pedidos_from_meli($limit = '20', $offset = '0')
    {
		$url = MELI_API_ROOT_URL.'/orders/search?seller='.$this->session->meli_user["meli_user_id"].'&limit='.$limit.'&offset='.$offset;
		
		if(is_string($this->filtro))
            $url .= '&' . $this->filtro;
        else
			$url .= '&' .  '&sort=date_desc';
			
		//print_r($_GET);
		//print_r(array_column($_GET['order_status'], 'order_status'));
		//var_dump(array_search("paid", $_GET["order_status"]));
		//var_dump(array_search("cancelled", $_GET["order_status"]));
		//die;

		$response = $this->meli->get($url);
		

        // @todo verificar se houve uma resposta válida. Em caso negativo, 
        // checar a troca do token com o método abaixo 
        // $this->check_config();


        if(isset($response['body']->error) || !isset($response['body']))
        {
			$this->meli_model->refresh_token();

            redirect('pedidos/lista', 'refresh');
        }
            
        
		$pedidos = array();
		
		if(!count($response["body"]->results))
			return $pedidos;

        $pedidos = $response["body"]->results;

        return $pedidos;
	}


}
