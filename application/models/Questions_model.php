<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions_model extends CI_Model {


	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
			$this->meli_user_id = isset($this->session->userdata["meli_user"]["meli_user_id"])?$this->session->userdata["meli_user"]["meli_user_id"]:null;
    }

    public function set_filtro($filtros = array())
	{
		foreach($filtros as $filtro => $valor)
		{
			$filtro = str_replace("_", ".", $filtro);
			if(is_array($valor))
				$this->filtro[] = $filtro . '=' . implode('|', $valor);
			else	
				$this->filtro[] = $filtro . '=' . $valor;
		}
		$this->filtro = implode('&', $this->filtro);

		return $this;

    }
    
    
    public function get_user_questions($limit = '20', $offset = '0')
    {
        $url = MELI_API_ROOT_URL.'/questions/search?seller_id='.$this->session->meli_user["meli_user_id"].'&status=unanswered&api_version=4&limit='.$limit.'&offset='.$offset;
		
		$response = $this->meli->get($url);

        if(isset($response['body']->error))
        {
			$this->meli_model->refresh_token();
			return $this->get_user_questions($limit, $offset);
		}

		$this->load->model("anuncios_model");
		//print_r($response['body']->questions);
		foreach($response['body']->questions as $key => $question)
		{
			$item = $this->anuncios_model->get_anuncio_detalhes(array($question->item_id));
			//print_r($response['body']->questions[$key]);
			//die;
			$response['body']->questions[$key]->item = $item[0]->body;
			
		}
		
		
		return $this->clean_questions_list($response);
	}
	
	/**
	 * Método que remove as perguntas que estão marcadas como deletadas
	 * da lista. deleted_from_listing==1
	 * Se não forem removidas, essas perguntas aparecem na lista embora o vendedores
	 * tenha marcada para exclusão, ou seja, não aparece na lista de perguntas do Mercadolivre
	 *
	 * @param [array] $response
	 * @return array
	 */
	private function clean_questions_list($response)
	{
		$questions = $response["body"]->questions;

		foreach($questions as $key => $question)
		{
			if($question->deleted_from_listing==1)
			{
				unset($questions[$key]);
			}
		}

		return $questions;
	}


	public function answer_question()
	{
		$body["question_id"] = strip_tags($this->input->post("question_id"));
		$body["text"] = strip_tags($this->input->post("text"));

		$url = MELI_API_ROOT_URL.'/answers';
		
        $response = $this->meli->post($url, $body);

        if(isset($response['body']->error) && $response['httpCode'] != 400)
        {
			$this->meli_model->refresh_token();
			return $this->answer_question();
		} 
		elseif($response['httpCode'] == 400)
		{
			throw new Exception($response['body']->message);
		}

	}
	
	/**
	 * Metodo que recebe uma notificação como parâmetro
	 * para que seja verificada a alteração
	 * Os dados da notificação são utilizados na chamada à API do Mercado Livre
	 * para pegar os dados atualizados da venda
	 *
	 * @param [type] $notification
	 * @return void
	 */
	public function notify_change($notification)
	{
		//print_r($notification);
		$url = MELI_API_ROOT_URL.$notification["resource"];
		$params = $this->meli_model->login_meli($notification["user_id"]);
		
		$response = $this->meli->get($url);

		if(isset($response['body']->error))
        {
			$this->meli_model->refresh_token();
			return $this->notify_change($notification);
		}

		$this->update_question_changes($response);
	}

	/**
	 * Método que atualiza a venda a partir da notificação de alteração
	 *
	 * @param [type] $venda
	 * @return void
	 */
	private function update_question_changes($question)
	{
		$result = $this->mongo_db
		->where('id', $question["body"]->id)
		->set((Array)$question)
		->update('questions', array('upsert' => true));

        /*
		$select = $this->mongo_db
		->where('id', $question["body"]->id)
		->get('questions');
        */

        $this->api_model->update_notification_result($result);
	}


}
