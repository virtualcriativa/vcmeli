<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

    private $date_from;
    private $date_until;
    private $meli_user_id;
    private $notifications;

    private $period;
    private $period_type;


	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
			//$this->meli_user_id = $this->session->userdata["meli_user"]["meli_user_id"];
    }
    
    public function set_period($period)
    {
        if((int)$period > 24)
            $period = 24;

        $this->period = $period;
    }

    public function set_period_type($set_period_type)
    {
        $this->set_period_type = $set_period_type;
    }

    /**
     * Informa a data de início dos dados
     *
     * @param [string] $date
     * @return void
     */
	public function set_date_from($date)
	{
        $this->date_from = $date;
    }
    
    /**
     * Informa a data final da coleta dos dados
     *
     * @param [string] $date
     * @return void
     */
    public function set_date_until($date)
	{
        $this->date_until = $date;
    }
    
    /**
     * Informa o ID (MLB) do anúncio a ser pesquisado
     *
     * @param [string] $ad
     * @return void
     */
    public function set_ad($ad)
	{
        $this->ad = $ad;
    }
    

    public function get_ad_metrics()
    {
        if(!$this->session->meli_user["meli_user_id"])
		{
			$data = array();
			return;
		}

        $visits = $this->get_visits();
        $sales = $this->get_sales();
        $days = $this->get_days();

        //print_r($visits);die;
        $conversion_rate = array();
        foreach($sales["vendas"] as $key => $sale){
            $conversion_rate["conversion_rate"][$key] = ($visits["visitas"][$key]==0)?0:($sale / $visits["visitas"][$key]*100);
            $conversion_rate["conversion_rate"][$key] = number_format($conversion_rate["conversion_rate"][$key], 2, '.', ',');
        }

        $metrics = array_merge($days, $visits, $sales, $conversion_rate);


        echo json_encode($metrics);

    }

    private function get_visits()
    {
        $url = MELI_API_ROOT_URL.'/items/'.$this->ad.'/visits/time_window?last=30&unit=&ending='.$this->date_until.'T23:59:59.000-00:00';

        $results = $this->meli->get($url);
        $results = $results['body']->results;


        $intervalos = $this->getDatesFromRange();

        foreach($intervalos as $dia)
        {
            $stats[$dia]["visitas"] = 0;
            $stats[$dia]["data"] = $dia;

            foreach($results as $key => $result)
            {
                $date = explode("T", $result->date);
                $date = date('Y-m-d', strtotime($date[0]));
                if (isset($stats[$date]))
                {
                    $stats[$date]["visitas"] += (int)$result->total;
                    unset($results[$key]);
                } 
            }
        }

        //$visits["dias"] = array_column($stats, "data");
        $visits["visitas"] = array_column($stats, "visitas");

        return $visits;
    }

    private function get_sales()
    {
        $url = MELI_API_ROOT_URL.'/orders/search?limit=50&item='.$this->ad.'&seller='.$this->session->meli_user["meli_user_id"].'&order.date_created.from='.$this->date_from.'T00:00:00.000-00:00&order.date_created.to='.$this->date_until.'T23:59:59.000-00:00&order.status=paid&sort=date_desc';

        $pedidos = $this->meli->get($url);
        $results = $pedidos['body']->results;


        if($pedidos['body']->paging->total > 50) 
        {
            $pages = ceil($pedidos['body']->paging->total / 50);

            for($i = 1;$i < $pages;$i++)
            {
                $offset = 50 * $i;
                $url = MELI_API_ROOT_URL.'/orders/search?offset='.$offset.'&limit=50&item='.$this->ad.'&seller='.$this->session->meli_user["meli_user_id"].'&order.date_created.from='.$this->date_from.'T00:00:00.000-00:00&order.date_created.to='.$this->date_until.'T23:59:59.000-00:00&order.status=paid&sort=date_desc';

                $pedidos = $this->meli->get($url);
                $results = array_merge($results, $pedidos['body']->results);

            }
            

        }


        $intervalos = $this->getDatesFromRange();

        foreach($intervalos as $dia)
        {
            $stats[$dia]["faturamento"] = 0;
            $stats[$dia]["comissao"] = 0;
            $stats[$dia]["vendas"] = 0;
            $stats[$dia]["data"] = $dia;

            foreach($results as $key => $result)
            {
                $date = explode("T", $result->date_created);
                $date = date('Y-m-d', strtotime($date[0]));
                if (isset($stats[$date]))
                {
                    $stats[$date]["vendas"] += (int)$result->order_items[0]->quantity;
                    $stats[$date]["faturamento"] += (int)$result->order_items[0]->full_unit_price * (int)$result->order_items[0]->quantity;
                    $stats[$date]["comissao"] += (int)$result->order_items[0]->sale_fee * (int)$result->order_items[0]->quantity;
                    unset($results[$key]);
                } 
            }
        }

        //$sales["dias"] = array_column($stats, "data");
        $sales["vendas"] = array_column($stats, "vendas");
        $sales["faturamento"] = array_column($stats, "faturamento");
        $sales["comissao"] = array_column($stats, "comissao");

        return $sales;
    }

    private function get_days()
    {

        $intervalos = $this->getDatesFromRange();

        foreach($intervalos as $dia)
        {
            $stats[$dia]["data"] = $dia;
        }

        $dias["dias"] = array_column($stats, "data");
        
        return $dias;
    }

    private function getDatesFromRange($format = 'Y-m-d') {
      
        // Declare an empty array
        $array = array();
          
        // Variable that store the date interval
        // of period 1 day
        $interval = new DateInterval('P1D');
      
        $realEnd = new DateTime($this->date_until);
        $realEnd->add($interval);
      
        $period = new DatePeriod(new DateTime($this->date_from), $interval, $realEnd);
      
        // Use loop to store date into array
        foreach($period as $date) {                 
            $array[] = $date->format($format); 
        }
      
        // Return the array elements
        return $array;
    }

    public function get_notification()
    {
        return $this->notification;
    }
    

    public function set_notification($notification)
    {
        $this->notification = $notification;
    }

    public function save_notification($json)
    {
        $json = json_decode($json);
        //print_r((array)$json);
        $notification = $this->mongo_db
        ->insert('notifications', (Array)$json);
       
        $this->set_notification($notification);
        $this->execute_notification();
    }

    public function get_notifications()
    {
        $notifications = $this->mongo_db
			->limit(50)
            ->get('notifications');
            
        $this->notifications = $notifications;

        return $this;
    }

    /**
     * Método que executa as notifcações
     *
     * @return void
     */
    public function execute_notifications()
    {
        $this->load->library('meli');
        $this->load->model('questions_model');
        $this->load->model('meli_model');
        $this->load->model('pedidos_model');
 
        foreach($this->notifications as $key => $notification)
        {
            $this->set_notification($notification);
            $this->execute_notification();
        }

    }


    private function execute_notification()
    {
        $notification = (array)$this->notification;
        $this->load->model('meli_model');
        $meli_user = $this->meli_model->login_meli($notification["user_id"]);
        $this->load->model('questions_model');
        $this->load->model('pedidos_model');

        $resource = $notification["resource"];
        
        echo "<br>";
        print_r($this->session->userdata);
        
        /*
        @todo Colocar todos os tipos de notificações para que 
        seja executado o método correto para cada uma
        */
        echo "<br>";
        print_r($notification["topic"]);
        echo "<br>";

        if($notification["topic"] == "orders_v2")
        {
            $this->pedidos_model->notify_change($notification);
        }
        elseif($notification["topic"] == "questions")
        {
            $this->questions_model->notify_change($notification);
        }
        else
        {
            $this->delete_notification();
        }
        
        
        $url = MELI_API_ROOT_URL.$resource;

        $params = array();
        $response = $this->meli->get($url, $params);
        //print_r($response);
    }

    private function delete_notification()
    {
        $notification = (Array)$this->get_notification();
        print_r("teste");
        print_r((Array)$notification);
        $_id = $notification["_id"]->{'$id'};

        $this->mongo_db
        ->where('_id', new MongoDB\BSON\ObjectId($_id))
        ->delete('notifications', $notification);

        unset($notification["_id"]);
        $this->mongo_db
        ->insert('notifications_deleted', $notification);
    }

    /**
     * Metódo que verifica se houve erro na atualização da notificação.
     * Caso não tenha erro, então a notificação é deletada da coleção e inserida
     * na coleção de notificações deletadas para mantermos o histórico
     *
     * @param [type] $result
     * @return void
     */
    public function update_notification_result($result)
    {
        if(count($result->getWriteErrors()) == 0)
        {
            $this->delete_notification();
            
        }
    }
	
}


