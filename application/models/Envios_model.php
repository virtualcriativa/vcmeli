<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envios_model extends CI_Model {

	private $limit_db_results = 10;
	private $filtro = array();


	public function __construct()
	{
			//$this->load->database();
			$this->load->library('session');
			//$this->load->library('mongo_db');

			$this->load->model('meli_model');
			$params = $this->meli_model->get_meli_params();
			$this->load->library('meli', $params);
	}
	
    public function get_envio($id)
    {
		$url = MELI_API_ROOT_URL.'/shipments/'.$id.'/costs';
		
		$response = $this->meli->get($url);

        $envios = array();

		if(!$response["body"]->gross_amount)
			return $envios;

        $envios = $response["body"];

        return $envios;
	}


}
