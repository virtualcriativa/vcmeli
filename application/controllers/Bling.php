<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bling extends CI_Controller {

	 
	public function __construct()
	{
			parent::__construct();

			$this->load->library('session');
			$this->load->helper(array('url'));
			$this->load->model('bling_model');
			
			$this->load->model('user_model');
			$this->user_model->is_logged();
			
	}

	public function register()
	{
		$this->bling_model->register();
	}
	
}
