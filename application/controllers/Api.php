<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            //$this->load->model('user_model');
            //$this->user_model->is_logged();
			//$this->load->model('pedidos_model');
            //$this->load->model('meli_model');
            //$this->load->model('envios_model');
            //$this->load->model('anuncios_model');
            //$this->load->model('api_model');
			//$this->user_model->is_trial_expired();
	}

    public function get_ad_metrics()
    {
        $date_from  = $_GET["date_from"];
        $date_until = $_GET["date_until"];
        $ad         = $_GET["ad"];

        $this->load->model('api_model');
        $this->api_model->set_date_from($date_from); 
        $this->api_model->set_date_until($date_until); 
        $this->api_model->set_ad($ad); 

        $this->api_model->get_ad_metrics(); 

    }


    public function notifications()
    {
        //echo "teste";

        $json = file_get_contents('php://input');
        //file_put_contents('novanotificacao.txt', $json, FILE_APPEND);
        
        /*
        //print_r($json);
        $pedido = $_POST;
        $output = print_r($_POST, true);
        file_put_contents('notificacao.txt', $json, FILE_APPEND);
        */
        
        $this->load->model('api_model');
        try
        {
            $this->api_model->save_notification($json);
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
        
    }

    /**
     * Métodos que executa as notificações do Mercado Livre que estão salvas no vanco de dados
     *
     * @return void
     */
    public function get_notifications()
    {
        //if($_SERVER["HTTP_X_CRON_AUTH"] != "8cb9b6313c62937b805aedea3f7fbae1"){
        //    die("Acesso nao Autorizado");
        //}

        $this->load->model('api_model');
        $this->api_model->get_notifications()->execute_notifications();
    }
	
}
