<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anuncios extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            $this->load->model('user_model');
            $this->user_model->is_logged();
			$this->load->model('anuncios_model');
			$this->load->model('meli_model');
			$this->user_model->is_trial_expired();
	}

	/**
	 * Método que exibe a lista de anúncios paginada
	 * Recebe o parâmetro $pagina com o número da página atual
	 *
	 * @param integer $pagina
	 * @return void
	 */
	public function lista($pagina = 0)
	{
		if(count($_GET))
			$this->anuncios_model->set_filtro($_GET);
		else
			$this->anuncios_model->set_filtro(array("orders"=>array("total_sold_quantity_desc")));
	

		// recebe a lista de anúncios diretamente do Mercado Livre
		
		$data = $this->anuncios_model->get_anuncios_from_meli(10, $pagina);

		$data['anuncios'] = $data;

		// recebe o número total de anúncios para enviar à paginação
		$total_anuncios = $this->anuncios_model->get_total_anuncios();
		$data["anuncios_total"] = $total_anuncios;

		// PAGINAÇÃO DOS ANÚNCIOS
		$this->load->library('pagination');
		$this->config->load('pagination', TRUE);
		
		// as configurações gerais da paginação estão no arquivo pagination.php na pasta de configuração do app
		$config = $this->config->item('pagination');
		$config['base_url'] = base_url() . "/anuncios/lista";
		$config['total_rows'] = $total_anuncios;
		$config['per_page'] = 10;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		// carrega a view
		$this->load->view('anuncios', array("anuncios"=>$data['anuncios'], "paginacao" => $data["links"], "anuncios_total" => $data["anuncios_total"]));
	}

	public function performance($id)
	{
		$this->load->model('bling_model');
		$anuncio 		= $this->anuncios_model->get_anuncio_detalhes(array($id));
		$anuncio 		= (array)$anuncio[0]->body;
		$descricao 		= $this->anuncios_model->get_anuncio_descricao($id);
		//$atributos 		= $this->anuncios_model->get_categoria_atributos($anuncio["category_id"]);
		$category_details = $this->anuncios_model->category_details($anuncio["category_id"]);
		
		$best_sellers 	= $this->anuncios_model->best_sellers($anuncio["category_id"]);
		if($best_sellers)
		{
			$best_sellers = array_column($best_sellers, 'id');
			$best_sellers	= $this->anuncios_model->get_anuncio_detalhes($best_sellers);
			foreach ($best_sellers as $key => $best_seller) {
				if($best_seller->code!="200")
				unset($best_sellers[$key]);
			}
			$best_sellers = array_slice($best_sellers, 0, 12);
		}
		else
		{
			$best_sellers = array();
		}
		
		$listing_position = $this->anuncios_model->listing_position($id);
		$visitas 		= $this->anuncios_model->get_total_visitas($id);

		$custos = $this->bling_model->get_custo($anuncio);

		$anuncio["descricao"] = $descricao;

		$this->load->view('anuncios/anuncio_performance', array("anuncio" => $anuncio, "visitas" => $visitas, "model" => $this->anuncios_model, "listing_position" =>$listing_position, "best_sellers" => $best_sellers, "category_details" => $category_details));
	}

	public function edit($id)
	{
		$anuncio = $this->anuncios_model->get_anuncio_detalhes(array($id));
		$anuncio = (array)$anuncio[0]->body;
		$descricao = $this->anuncios_model->get_anuncio_descricao($id);
		$atributos = $this->anuncios_model->get_categoria_atributos($anuncio["category_id"]);

		$anuncio["descricao"] = $descricao;

		$this->load->view('anuncio_detalhes', array("anuncio" => $anuncio, "atributos" => $atributos, "model" => $this->anuncios_model));
	}

}
