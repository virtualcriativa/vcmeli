<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mercadolivre extends CI_Controller {

	 
	public function __construct()
	{
			parent::__construct();

            $this->load->model('user_model');
            $this->load->model('meli_model');
			$this->user_model->is_logged();
			
	}

	public function dashboard()
	{
		$this->load->helper(array('url'));
		$this->load->view('dashboard');
    }
	

    public function add_account()
	{
		$this->load->helper('url');
		$this->meli_model->send_to_auth();
		//redirect('/account/login', 'refresh');
	}

	public function authorize_meli()
	{
		$user = $this->meli_model->authorize_meli($_GET['code']);
		redirect('/dashboard');
	}

	public function performance()
	{
		//$data["total_visits"] 		= $this->anuncios_model->get_total_visits($m, $y);
		//$data["total_sales_number"] = $this->pedidos_model->get_total_sales_number($m, $y);
		
		//$user = $this->meli_model->load_performance();
		$this->load->view('account/performance');
	}

	public function load_performance()
	{
		$this->load->model('anuncios_model');
		$this->load->model('pedidos_model');

		$period_type 	= $_GET["period_type"];
		$period 		= $_GET["period_number"];
		$date_from 		= $_GET["date_from"];
		$date_from 		= new DateTime($date_from);

		if($period_type == "W") 
		{
			$date_to = clone $date_from;
			$date_to->add(new DateInterval("P1W"));
		}
		elseif($period_type == "M")
		{
			//$date_from->add(new DateInterval("P1M"));
			$date_from->modify('first day of this month');
			$date_to = clone $date_from;
			$date_to->modify('last day of this month');
		}
		elseif($period_type == "D")
		{
			//$date_from->add(new DateInterval("P1M"));
			
			$date_to = clone $date_from;
			$date_to->add(new DateInterval("P1D"));
		}

		/* $interval_str = "P".$period.$period_type;
		$date_from = new DateTime ();
		$date_from->sub(new DateInterval($interval_str));

		$interval_str = "P1".$period_type;
		$date_to = clone $date_from;
		$date_to->add(new DateInterval($interval_str)); */
		

		for($i=1;$period;$i++)
		{
			$this->anuncios_model->set_start_date($date_from);
			$this->anuncios_model->set_end_date($date_to);

			$this->pedidos_model->set_start_date($date_from);
			$this->pedidos_model->set_end_date($date_to);
			
			$visits 	= $this->anuncios_model->get_total_visits();
			$sales 		= $this->pedidos_model->get_total_sales_number();
			$perc 		=  ((int)$visits >0)?(int)$sales/(int)$visits*100:0;

			$data["total_visits"][] 		= $visits;
			$data["total_sales_number"][] 	= $sales;
			$data["conversion_rate"][] 		= number_format($perc, 2, '.', ',');
			$data["dias"][] 				= $date_from->format('d-m-Y') ." até ". $date_to->format('d-m-Y');

			if($period==$i)
				break;
			
			//$date_from = clone $date_to;
			$date_from->add(new DateInterval("P1".$period_type));
			if($period_type=="M")
			{
				$date_to = clone $date_from;
				$date_to->modify('last day of this month');
			}
			else
			{
				$date_to = clone $date_from;
				$date_to->add(new DateInterval("P1".$period_type));
			}
			
			
		}

		echo json_encode($data);
		

		
		// $this->load->model('api_model');
        // $this->api_model->set_period($period); 
        // $this->api_model->set_period_type($period_type); 

        // $this->api_model->load_performance(); 
	}
	
}
