<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	private $id_usuario;

	public function __construct()
	{
			parent::__construct();
			$this->load->model('user_model');
			$this->load->library('ion_auth');
			$this->user_model->is_logged();
			//$this->user_model->is_trial_expired();
	}

	public function update()
	{
		$id = $_POST["id_usuario"];
		$id_compare = $this->session->userdata("user_id");

		// Adicionamos uma camada de segurança ao comparar o id do form com o id do usuário da sessão
		if($id != $id_compare){
			$this->session->set_flashdata('message', 'Houve um problema ao atualizar os dados. Tente novamente mais tarde.');
			redirect('user/account');
			return;
		}

		$data = array(
			'first_name' => $_POST["first_name"],
			'last_name' => $_POST["last_name"],
			'phone' => $_POST["phone"],
			'username' => $_POST["email"],
			'email' => $_POST["email"],
			'password' => $_POST["password"]
			);

		$additional_data = array(
			'endereco' => $_POST["endereco"],
			'numero' => $_POST["numero"],
			'complemento' => $_POST["complemento"],
			'bairro' => $_POST["bairro"],
			'cidade' => $_POST["cidade"],
			'cep' => $_POST["cep"],
			'estado' => $_POST["estado"]
			);


		$ret = $this->ion_auth->update($id, $data);
		$this->user_model->update_user_info($id, $additional_data);
		$this->session->set_flashdata('message', 'Dados atualizados com sucesso.');
		redirect('user/account');
	}

	public function account()
	{
		$user 		= $this->ion_auth->user()->row();
		$additional = $this->user_model->get_user_data();		
		$user= array_merge((array)$user, (array)$additional);

		$this->load->view('account', $user);
	}
	
}
