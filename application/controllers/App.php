<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	 
	public function __construct()
	{
			parent::__construct();

			$this->load->library('session');
			$this->load->model('user_model');
			$this->load->model('anuncios_model');
			$this->load->model('pedidos_model');
			$this->load->helper(array('url'));
			$this->user_model->is_logged();
			
	}

	public function dashboard()
	{
		$y = date("Y");
		$m = date('m');

		$date_to = new DateTime ();
		$date_from = new DateTime('first day of this month');
		$this->anuncios_model->set_start_date($date_from);
		$this->anuncios_model->set_end_date($date_to);

		$this->pedidos_model->set_start_date($date_from);
		$this->pedidos_model->set_end_date($date_to);
		
		try
		{
			$data["total_visits"] 		= $this->anuncios_model->get_total_visits();
			$data["total_sales_number"] = $this->pedidos_model->get_total_sales_number();
			$data["anuncios_total"] 	= $this->anuncios_model->get_total_anuncios();
			$data["custo_mensal"]		= $this->pedidos_model->get_vendas_produtos_custos($m, $y);
			$data["venda_mensal"] 		= $this->pedidos_model->get_vendas_mensais($m, $y);
			$data["comissao_mensal"] 	= $this->pedidos_model->get_comissoes_mensais($m, $y);
			$data["lucro_mensal"] 		= $this->pedidos_model->get_lucro_mensal($m, $y);
			$data['mais_vendidos'] 		= $this->anuncios_model->get_mais_vendidos(5);
			$data['estatisticas'] 		= $this->anuncios_model->get_stats();
		} 
		catch (Exception $e) 
		{
			$this->load->view('errors/token_error', array("message" => $e->getMessage()));
			return;
		}
		
		$this->load->view('dashboard', $data);
	}

	public function integracoes()
	{
		$this->load->helper('form');
		$this->load->model('bling_model');

		$data["apiconfig"] = $this->bling_model->get_apikey_config();
		$this->load->view('integracoes', $data);
	}
	
}
