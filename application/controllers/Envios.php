<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Envios extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            //$this->user_model->is_logged();
			$this->load->model('envios_model');
			//$this->user_model->is_trial_expired();
	}
	/**
	 * Método que exibe a lista de anúncios paginada
	 * Recebe o parâmetro $pagina com o número da página atual
	 *
	 * @param integer $pagina
	 * @return void
	 */
	public function get_envio($id)
	{

		$return = $this->envios_model->get_envio($id);
		echo json_encode($return);
	}

}
