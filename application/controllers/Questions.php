<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            $this->load->model('user_model');
            $this->user_model->is_logged();
			$this->load->model('questions_model');
            $this->load->model('meli_model');
			$this->load->helper('form');
			$this->user_model->is_trial_expired();
	}


	public function list($pagina = 0)
	{
		// recebe a lista de perguntas diretamente do Mercado Livre
		$data = $this->questions_model->get_user_questions();

		// carrega a view
		$this->load->view('questions/list', array("questions"=>$data, "message" => $this->session->flashdata('message')));
	}

	public function send_answer()
	{
		try
		{
			$data = $this->questions_model->answer_question();
			
		}
		catch(Exception $e)
		{
			$this->session->set_flashdata('message', $e->getMessage());
			redirect("questions/list", 'refresh');
			
		}

		$this->session->set_flashdata('message', 'Pergunta respondida com sucesso');
		redirect("questions/list", 'refresh');

		
	}

	
}
