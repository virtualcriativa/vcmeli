<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {

	public function __construct()
	{
			parent::__construct();
            $this->load->model('user_model');
            $this->user_model->is_logged();
			$this->load->model('pedidos_model');
            $this->load->model('meli_model');
            $this->load->model('envios_model');
			$this->load->model('anuncios_model');
			$this->load->helper('form');
			$this->user_model->is_trial_expired();
	}


	/**
	 * Método que exibe a lista de produtos vendidos paginada
	 * Recebe o parâmetro $pagina com o número da página atual
	 *
	 * @param integer $pagina
	 * @return void
	 */
	public function produtos_vendidos($pagina = 0)
	{

		if(count($_GET))
			$this->pedidos_model->set_filtro($_GET);

					
		// recebe a lista de anúncios diretamente do Mercado Livre
        $data = $this->pedidos_model->get_pedidos_from_meli(10, $pagina);
        
        foreach($data as $index => $pedido)
        {
            
            $envio = $this->envios_model->get_envio($pedido->shipping->id);
            $data[$index]->shipping->info = $envio;
            foreach($pedido->order_items as $o_index => $order_items)
            {   
                $image = $this->anuncios_model->get_anuncio_imagens($order_items->item->id);
                $data[$index]->order_items[$o_index]->thumbnail = $image;
            }
        }
 
        $data['pedidos'] = $data;

		// recebe o número total de anúncios para enviar à paginação
		$total_pedidos = $this->pedidos_model->get_total_pedidos();
        $data["total_pedidos"] = $total_pedidos;
   
		// PAGINAÇÃO DOS ANÚNCIOS
		$this->load->library('pagination');
		$this->config->load('pagination', TRUE);
		
		// as configurações gerais da paginação estão no arquivo pagination.php na pasta de configuração do app
		$config = $this->config->item('pagination');
		$config['base_url'] = base_url() . "/pedidos/produtos_vendidos";
		$config['total_rows'] = $total_pedidos;
		$config['per_page'] = 10;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		// carrega a view
		$this->load->view('produtos_vendidos', array("pedidos"=>$data['pedidos'], "paginacao" => $data["links"], "pedidos_total" => $data["total_pedidos"]));
	}

	/**
	 * Método que exibe a lista de produtos vendidos paginada
	 * Recebe o parâmetro $pagina com o número da página atual
	 *
	 * @param integer $pagina
	 * @return void
	 */
	public function lista($pagina = 0)
	{


		$date_to = new DateTime ();
		$date_from = new DateTime('first day of this month');

		$this->pedidos_model->set_start_date($date_from);
		$this->pedidos_model->set_end_date($date_to);

					
		// recebe a lista de anúncios diretamente do Mercado Livre
		//$data = $this->pedidos_model->get_pedidos_from_meli(1, $pagina);
        $data = $this->pedidos_model->get_sales_from_range(1, $pagina);

        foreach($data as $index => $pedido)
        {
            $envio = $this->envios_model->get_envio($pedido->shipping->id);
            $data[$index]->shipping->info = $envio;
        }
 
        $data['pedidos'] = $data;

		// recebe o número total de anúncios para enviar à paginação
		$total_pedidos = $this->pedidos_model->get_total_pedidos();
        $data["total_pedidos"] = $total_pedidos;
   
		// PAGINAÇÃO DOS ANÚNCIOS
		$this->load->library('pagination');
		$this->config->load('pagination', TRUE);
		
		// as configurações gerais da paginação estão no arquivo pagination.php na pasta de configuração do app
		$config = $this->config->item('pagination');
		$config['base_url'] = base_url() . "/pedidos/lista";
		$config['total_rows'] = $total_pedidos;
		$config['per_page'] = 10;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		// carrega a view
		$this->load->view('lista', array("pedidos"=>$data['pedidos'], "paginacao" => $data["links"], "pedidos_total" => $data["total_pedidos"]));
	}

	public function relatorio()
	{
		$y = date("Y");
		$m = date('m');

		$data["lucro_mensal"] 		= $this->pedidos_model->get_lucro_mensal($m, $y);
		
		$this->load->view('pedidos/relatorio', $data);
	}

	public function edit($mlb)
	{
		print_r($mlb);
		print_r($this->session);
		
		
		$this->load->library('meli', array('client_id' => "3848527369621555", 'client_secret' => "VpLdGUVY5UHL8gtwk1kM4hmXuNSkxza5"));
		$params = array('access_token' => $this->session->meli_access_token);
        $response = $this->meli->get('items/'.$mlb.'?access_token=APP_USR-3848527369621555-112220-47b0060196d4e50b94baefd21029b7ee-96454909');
		print_r($response);
		//return $response["body"]->paging->total;

		$this->load->view('anuncios_edit', $data);
	}

	public function datatable_list()
	{
		$data = $this->pedidos_model->get_anuncios();
		$total = count($data);
		$draw 		= (isset($_GET['draw']))	?$_GET['draw']:NULL;
		$start 		= (isset($_GET['start']))	?$_GET['start']:NULL;
		$length 	= (isset($_GET['length']))	?$_GET['length']:NULL;
		$search 	= (isset($_GET['search']['value']))	?$_GET['search']['value']:NULL;
		$order 		= (isset($_GET['order']))	?$_GET['order']:NULL;
		$columns 	= (isset($_GET['columns']))	?$_GET['columns']:NULL;
		$like_search = "";
		
		$return 					= array();
		$return['draw'] 			= $draw;
		$return['recordsTotal'] 	= $total;
		$return['recordsFiltered'] 	= $total;
		$return['data'] 			= array();
		
		foreach($data as $anuncio)
		{
			$return['data'][] = array(
				'Img'=>$anuncio["anuncio"]->secure_thumbnail,
				'id'=>$anuncio["anuncio"]->id,
				'SKU'=>$anuncio["anuncio"]->seller_custom_field, 
				'produto'=>$produto, 
				"preco"=>"R$".number_format($americanas['preco'], 2, ',', '.')."<br>".$preco_detalhes, 
				"ordem"=>$americanas['ordem'], 
				);
		}

		echo json_encode($return);
	}
	
}
